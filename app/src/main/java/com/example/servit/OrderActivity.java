package com.example.servit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.servit.adapter.ExtraServiceAdapter;
import com.example.servit.adapter.ServiceOrderAdapter;
import com.example.servit.model.ExtraServiceModel;
import com.example.servit.model.OrderCategoryModel;
import com.example.servit.model.ServiceModel;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class OrderActivity extends AppCompatActivity implements ExtraDialog.extraDialogListener {


    private DatePickerDialog datePickerDialog;
    private MaterialButton dateButton, timeButton;
    private static final String TAG = "OrderActivity";
    private ArrayList<OrderCategoryModel> orderCategoryModels;
    private ArrayList<ExtraServiceModel> extraServiceModels;
    DatabaseReference mOrderDatabase, mExtraDatabase;
    FirebaseDatabase fData;
    RecyclerView recyclerViewOrder, recyclerViewExtraService;
    ServiceOrderAdapter serviceOrderAdapter;
    ExtraServiceAdapter extraServiceAdapter;
    ValueEventListener valueEventListener;
    TextView txt_total_uang, txt_uang_extra, txt_uang_service, txtTitle;
    ImageView arrow_icon;
    private int currentIndex;
    int currentServicePrice = 0;
    int currentOtherServicePrice = 0;
    int currentExtraPrice = 0;
    int currentTotal;
    ArrayList<String> list;
    ArrayList<Integer> list2;
    int hour, minute;
    String date, time, serviceName, extraName, catName;
    Integer otherServiceNumber = 0;
    String otherServiceName = "";



    public OrderActivity() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        MaterialToolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initDatePicker();

        txtTitle = findViewById(R.id.toolbar).findViewById(R.id.txt_name);
        txtTitle.setText("Choose Service");

        dateButton = findViewById(R.id.btn_date);
        timeButton = findViewById(R.id.btn_time);

        fData = FirebaseDatabase.getInstance();
        mOrderDatabase = fData.getReference("order");
        mExtraDatabase = fData.getReference("extraService");

        orderCategoryModels = new ArrayList<>();
        extraServiceModels = new ArrayList<>();
        list = new ArrayList<>();
        list2 = new ArrayList<>();

        recyclerViewOrder = findViewById(R.id.recycles_order);
        recyclerViewExtraService = findViewById(R.id.recycles_extra);

        txt_total_uang = findViewById(R.id.txt_total_uang);
        arrow_icon = findViewById(R.id.img_btn_next);

        recyclerViewOrder.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        recyclerViewExtraService.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
//        recyclerViewOrder.setAdapter(serviceOrderAdapter);


        serviceOrderAdapter = new ServiceOrderAdapter(getApplicationContext(), orderCategoryModels, new ServiceOrderAdapter.RecyclerItemClickListener() {
            @Override
            public void onClickListener(OrderCategoryModel orderCategoryModel, int position) {

                currentServicePrice = 0;

                if (!orderCategoryModel.getServiceName().equals("Others")) {

                    currentServicePrice = orderCategoryModel.getOrderPrice();
                    serviceName = orderCategoryModel.getServiceName();
                    catName = orderCategoryModel.getCatName();


                    changeSelectedService(position);

                    onPriceChanged(currentExtraPrice + currentServicePrice);

                } else {
                    currentServicePrice = orderCategoryModel.getOrderPrice();
//                    serviceName = otherServiceName;
                    catName = orderCategoryModel.getCatName();
                    DialogFragment dialogFragment = new ExtraDialog();
                    dialogFragment.show(getSupportFragmentManager(), "ExtraDialog");

                    Log.d(TAG, "Service NAme : " + serviceName);

                    changeSelectedService(position);

                    currentOtherServicePrice = otherServiceNumber * currentServicePrice;

                    onPriceChanged(currentExtraPrice + currentOtherServicePrice);

                }


            }

        });


        extraServiceAdapter = new ExtraServiceAdapter(getApplicationContext(), extraServiceModels, new ExtraServiceAdapter.RecyclerItemClickListener() {
            @Override
            public void onClickListener(ExtraServiceModel extraServiceModel, int position) {

//                currentExtraPrice = extraServiceModel.getExtraPrice();
//                extraName = extraServiceModel.getExtraServiceName();
                currentExtraPrice = 0;

                list = new ArrayList<>();
                list2 = new ArrayList<>();

                Log.d(TAG, "1 " + String.valueOf(currentExtraPrice));

                changeSelectedExtra(position);

                for (ExtraServiceModel model : extraServiceModels) {
                    if (model.isSelected()) {
                        Log.d(TAG, "2 " + String.valueOf(currentExtraPrice));
                        currentExtraPrice += model.getExtraPrice();
                        Log.d(TAG, "3 " + String.valueOf(currentExtraPrice));
//                        extraName += model.getExtraServiceName();
                        list.add(model.getExtraServiceName());
                        list2.add(model.getExtraPrice());
                        Log.d(TAG, "Name " + list);
                        Log.d(TAG, "Price " + list2);
                    }
//                    else if (!model.isSelected()){
//                        currentExtraPrice = 0;
//                    }
                }

                Log.d(TAG, "4 " + String.valueOf(currentExtraPrice));


                if (otherServiceNumber <= 1) {


                    onPriceChanged(currentExtraPrice + currentServicePrice);

                } else {

                    currentOtherServicePrice = otherServiceNumber * currentServicePrice;

                    onPriceChanged(currentExtraPrice + currentOtherServicePrice);
                }


            }
        });

        newActivity(currentTotal);

        valueEventListener = mOrderDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                orderCategoryModels.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    OrderCategoryModel orderCategoryModel = dataSnapshot.getValue(OrderCategoryModel.class);
                    orderCategoryModel.setmKey(dataSnapshot.getKey());
                    currentIndex = 0;

                    if (savedInstanceState != null) {
                        // Restore value of members from saved state
                        String catName = savedInstanceState.getString("catName");
                        if (catName.equals(orderCategoryModel.getCatName())) {

                            orderCategoryModels.add(orderCategoryModel);
                        }
                    } else {
                        final String s = getIntent().getExtras().getString("catName");
                        if (s.equals(orderCategoryModel.getCatName())) {

                            orderCategoryModels.add(orderCategoryModel);

                        }

                    }
                }

//                serviceOrderAdapter = new ServiceOrderAdapter(getApplicationContext(),orderCategoryModels);
                serviceOrderAdapter.setSelectedPosition(-1);
                recyclerViewOrder.setAdapter(serviceOrderAdapter);
                serviceOrderAdapter.setCatModels(ServiceOrderAdapter.getInstance().getAllOrder());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w(TAG, "helooooooooooooo" + error.getMessage());
            }
        });


        valueEventListener = mExtraDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                extraServiceModels.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    ExtraServiceModel extraServiceModel = dataSnapshot.getValue(ExtraServiceModel.class);
                    extraServiceModel.setmKey(dataSnapshot.getKey());
                    currentIndex = 0;

                    if (savedInstanceState != null) {
                        // Restore value of members from saved state
                        String catName = savedInstanceState.getString("catName");
                        if (catName.equals(extraServiceModel.getCatName())) {

                            extraServiceModels.add(extraServiceModel);
                        }
                    } else {
                        // Probably initialize members with default values for a new instance
                        final String a = getIntent().getExtras().getString("catName");
                        if (a.equals(extraServiceModel.getCatName())) {

                            extraServiceModels.add(extraServiceModel);
                        }
                    }

                }

                extraServiceAdapter.setSelectedPosition(-1);
                recyclerViewExtraService.setAdapter(extraServiceAdapter);
                extraServiceAdapter.setExtraModels(ExtraServiceAdapter.getInstance().getAllExtra());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w(TAG, "helooooooooooooo" + error.getMessage());
            }
        });


    }


    private void initDatePicker() {
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                date = makeDateString(dayOfMonth, month, year);
                dateButton.setText(date);
//                dateButton.setText(getTodaysDate());
            }
        };

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        int style = AlertDialog.THEME_HOLO_LIGHT;

        datePickerDialog = new DatePickerDialog(this, style, dateSetListener, year, month, day);
//        datePickerDialog.getDatePicker().setMinDate(Instant.now().toEpochMilli());
    }

    private String getTodaysDate() {

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        month = month + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);
        return makeDateString(day, month, year);
    }

    private String makeDateString(int dayOfMonth, int month, int year) {
        return getMonthFormat(month) + " " + dayOfMonth + " " + year;
    }

    private String getMonthFormat(int month) {
        if (month == 1) {
            return "JAN";
        } else if (month == 2) {
            return "FEB";
        } else if (month == 3) {
            return "MAR";
        } else if (month == 4) {
            return "APR";
        } else if (month == 5) {
            return "MAY";
        } else if (month == 6) {
            return "JUN";
        } else if (month == 7) {
            return "JUL";
        } else if (month == 8) {
            return "AUG";
        } else if (month == 9) {
            return "SEP";
        } else if (month == 10) {
            return "OCT";
        } else if (month == 11) {
            return "NOV";
        } else if (month == 12) {
            return "DEX";
        }
        return "Jan";
    }


    public void changeSelectedService(int index) {
        serviceOrderAdapter.notifyItemChanged(serviceOrderAdapter.getSelectedPosition());
        currentIndex = index;
        serviceOrderAdapter.setSelectedPosition(currentIndex);
        serviceOrderAdapter.notifyItemChanged(currentIndex);
    }


    public void changeSelectedExtra(int index) {
        extraServiceAdapter.notifyItemChanged(extraServiceAdapter.getSelectedPosition());
        currentIndex = index;
        extraServiceAdapter.setSelectedPosition(currentIndex);
        extraServiceAdapter.notifyItemChanged(currentIndex);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void openDatePicker(View view) {
        datePickerDialog.show();
    }

    public void openTimerPicker(View view) {
        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                hour = hourOfDay;
                minute = minute;
                time = String.format(Locale.getDefault(), "%02d:%02d", hourOfDay, minute);
                timeButton.setText(time);
            }
        };

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, onTimeSetListener, hour, minute, true);

        timePickerDialog.setTitle("Select Time");
        timePickerDialog.show();
    }


    public void onPriceChanged(int total) {

        txt_total_uang.setText("Rp. " + String.valueOf(total));

        currentTotal = total;


    }

    private void newActivity(int total) {
        currentTotal = total;
        arrow_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (date == null || time == null) {
                    dateButton.setError("please Enter Date");
                    timeButton.setError("please Enter Time");
                    dateButton.setFocusable(true);
                    dateButton.setFocusableInTouchMode(true);
                    dateButton.requestFocus();
                    timeButton.setFocusable(true);
                    timeButton.setFocusableInTouchMode(true);
                    timeButton.requestFocus();

                } else {

                    Toast.makeText(OrderActivity.this, list.toString(), Toast.LENGTH_SHORT).show();

                    Bundle bundle = new Bundle();
                    bundle.putString("catName", catName);
                    if (currentOtherServicePrice >= 1){
                        bundle.putInt("servicePrice", currentOtherServicePrice);
                    }else {
                        bundle.putInt("servicePrice", currentServicePrice);
                    }
                    bundle.putInt("extraPrice", currentExtraPrice);
                    bundle.putString("serviceName", serviceName);
                    bundle.putString("otherService", otherServiceName);
                    bundle.putStringArrayList("extraName", list);
                    bundle.putIntegerArrayList("extraListPrice", list2);
                    bundle.putInt("currentTotal", currentTotal);
                    bundle.putString("currentDate", date);
                    bundle.putString("currentTime", time);
                    Intent intent = new Intent(getApplicationContext(), OrderUserInfoActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {

        final String a = getIntent().getExtras().getString("catName");
        outState.putString("catName", a);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onSendValueClick(String value) {

        String regexStr = "^[1-9]\\d*(\\.\\d+)?$";


        serviceName = "";
        otherServiceNumber = 0;
        currentOtherServicePrice=0;

        if (value.matches(regexStr)) {

            otherServiceNumber = Integer.valueOf(value);
            serviceName = value + " Hour";
            onPriceChanged(currentServicePrice * otherServiceNumber);

        } else {

            otherServiceNumber = 1;
            serviceName = value;
            onPriceChanged(currentServicePrice);

        }

//        otherServiceNumber = Integer.valueOf(value);


        Log.d(TAG, otherServiceNumber.toString());
    }

}