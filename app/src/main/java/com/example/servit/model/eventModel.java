package com.example.servit.model;

public class eventModel {

    private String eventName, imageUrl, longDesc, id, title;

    public eventModel(String eventName, String imageUrl, String longDesc, String id, String title) {
        this.eventName = eventName;
        this.imageUrl = imageUrl;
        this.longDesc = longDesc;
        this.id = id;
        this.title = title;
    }

    public eventModel() {
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLongDesc() {
        return longDesc;
    }

    public void setLongDesc(String longDesc) {
        this.longDesc = longDesc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

