package com.example.servit.model;

public class OrderCategoryModel {
    String ServiceName,OrderDate, OrderTime, catName, mKey;
    Integer OrderPrice;


    public OrderCategoryModel(String serviceName, String orderDate, String orderTime, String catName, String mKey, Integer orderPrice) {
        ServiceName = serviceName;
        OrderDate = orderDate;
        OrderTime = orderTime;
        this.catName = catName;
        this.mKey = mKey;
        OrderPrice = orderPrice;
    }

    public OrderCategoryModel(){}

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public Integer getOrderPrice() {
        return OrderPrice;
    }

    public void setOrderPrice(Integer orderPrice) {
        OrderPrice = orderPrice;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public String getOrderTime() {
        return OrderTime;
    }

    public void setOrderTime(String orderTime) {
        OrderTime = orderTime;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getmKey() {
        return mKey;
    }

    public void setmKey(String mKey) {
        this.mKey = mKey;
    }
}
