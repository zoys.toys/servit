package com.example.servit.model;

import java.io.Serializable;

public class ServiceModel {
    String serviceName, serviceAddress, serviceProvide, serviceUrl, serviceBriefDesc, serviceLongDesc;
    Boolean favorite;

    public ServiceModel(String serviceName, String serviceAddress, String serviceProvide, String serviceUrl, String serviceBriefDesc, String serviceLongDesc, Boolean favorite) {
        this.serviceName = serviceName;
        this.serviceAddress = serviceAddress;
        this.serviceProvide = serviceProvide;
        this.serviceUrl = serviceUrl;
        this.serviceBriefDesc = serviceBriefDesc;
        this.serviceLongDesc = serviceLongDesc;
        this.favorite = favorite;
    }

    public ServiceModel(){

    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceAddress() {
        return serviceAddress;
    }

    public void setServiceAddress(String serviceAddress) {
        this.serviceAddress = serviceAddress;
    }

    public String getServiceProvide() {
        return serviceProvide;
    }

    public void setServiceProvide(String serviceProvide) {
        this.serviceProvide = serviceProvide;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public String getServiceBriefDesc() {
        return serviceBriefDesc;
    }

    public void setServiceBriefDesc(String serviceBriefDesc) {
        this.serviceBriefDesc = serviceBriefDesc;
    }

    public String getServiceLongDesc() {
        return serviceLongDesc;
    }

    public void setServiceLongDesc(String serviceLongDesc) {
        this.serviceLongDesc = serviceLongDesc;
    }
}
