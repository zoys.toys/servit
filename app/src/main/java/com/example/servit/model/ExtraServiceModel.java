package com.example.servit.model;

public class ExtraServiceModel {
    String catName, mKey, extraServiceName, imgUrl;
    Integer extraPrice;
    boolean isSelected = false;


    public ExtraServiceModel(String catName, String mKey, String extraServiceName, String imgUrl, Integer extraPrice) {
        this.catName = catName;
        this.mKey = mKey;
        this.extraServiceName = extraServiceName;
        this.imgUrl = imgUrl;
        this.extraPrice = extraPrice;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public ExtraServiceModel() {
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getmKey() {
        return mKey;
    }

    public void setmKey(String mKey) {
        this.mKey = mKey;
    }

    public String getExtraServiceName() {
        return extraServiceName;
    }

    public void setExtraServiceName(String extraServiceName) {
        this.extraServiceName = extraServiceName;
    }

    public Integer getExtraPrice() {
        return extraPrice;
    }

    public void setExtraPrice(Integer extraPrice) {
        this.extraPrice = extraPrice;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
