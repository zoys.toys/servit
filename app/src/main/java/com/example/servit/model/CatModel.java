package com.example.servit.model;

public class CatModel {
    String catName, iconUrl,serviceList;

    public CatModel(String catName, String iconUrl, String serviceList) {
        this.catName = catName;
        this.iconUrl = iconUrl;
        this.serviceList = serviceList;
    }

    public CatModel(){

    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getServiceList() {
        return serviceList;
    }

    public void setServiceList(String serviceList) {
        this.serviceList = serviceList;
    }
}
