package com.example.servit.model;

public class UserAccount {
    private String name,email,uid,imgUrl, address;
    private long phoneNumber;

    public UserAccount(String name, String email, String uid, long phoneNumber, String imgUrl, String address) {
        this.name = name;
        this.email = email;
        this.uid = uid;
        this.phoneNumber = phoneNumber;
        this.imgUrl = imgUrl;
        this.address = address;
    }

    public UserAccount(String email, long phone, String id, String name, String address) {
        this.email = email;
        this.uid = id;
        this.phoneNumber = phone;
        this.name = name;
        this.address = address;
    }

    public UserAccount(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "UserAccount{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", uid='" + uid + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", address='" + address + '\'' +
                ", phoneNumber=" + phoneNumber +
                '}';
    }
}
