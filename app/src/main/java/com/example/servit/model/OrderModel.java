package com.example.servit.model;

import java.util.ArrayList;

public class OrderModel {

    String orderId,userName, userAddress, userNote, userPhone, userID, serviceName, extraName, orderTime, orderDate, catName;
    Integer orderTotal, status, servicePrice, extraPrice;
    ArrayList<String> extraListName;
    ArrayList<Integer> extraListPrice;


    public OrderModel(String orderId, String userName, String userAddress, String userNote, String userPhone, String userID, String serviceName, String extraName, String orderTime, String orderDate, String catName, Integer orderTotal, Integer status, Integer servicePrice, Integer extraPrice, ArrayList<String> extraListName, ArrayList<Integer> extraListPrice) {
        this.orderId = orderId;
        this.userName = userName;
        this.userAddress = userAddress;
        this.userNote = userNote;
        this.userPhone = userPhone;
        this.userID = userID;
        this.serviceName = serviceName;
        this.extraName = extraName;
        this.orderTime = orderTime;
        this.orderDate = orderDate;
        this.catName = catName;
        this.orderTotal = orderTotal;
        this.status = status;
        this.servicePrice = servicePrice;
        this.extraPrice = extraPrice;
        this.extraListName = extraListName;
        this.extraListPrice = extraListPrice;
    }

    public OrderModel() {

    }

    public ArrayList<String> getExtraListName() {
        return extraListName;
    }

    public void setExtraListName(ArrayList<String> extraListName) {
        this.extraListName = extraListName;
    }

    public ArrayList<Integer> getExtraListPrice() {
        return extraListPrice;
    }

    public void setExtraListPrice(ArrayList<Integer> extraListPrice) {
        this.extraListPrice = extraListPrice;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserNote() {
        return userNote;
    }

    public void setUserNote(String userNote) {
        this.userNote = userNote;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getExtraName() {
        return extraName;
    }

    public void setExtraName(String extraName) {
        this.extraName = extraName;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Integer getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(Integer orderTotal) {
        this.orderTotal = orderTotal;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(Integer servicePrice) {
        this.servicePrice = servicePrice;
    }

    public Integer getExtraPrice() {
        return extraPrice;
    }

    public void setExtraPrice(Integer extraPrice) {
        this.extraPrice = extraPrice;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    @Override
    public String toString() {
        return "OrderModel{" +
                "userName='" + userName + '\'' +
                ", userAddress='" + userAddress + '\'' +
                ", userNote='" + userNote + '\'' +
                ", userPhone='" + userPhone + '\'' +
                ", userID='" + userID + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", extraName='" + extraName + '\'' +
                ", orderTime='" + orderTime + '\'' +
                ", orderDate='" + orderDate + '\'' +
                ", orderTotal=" + orderTotal +
                '}';
    }
}
