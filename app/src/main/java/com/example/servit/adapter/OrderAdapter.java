package com.example.servit.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemRecyclerMangerImpl;
import com.example.servit.ConfirmOrderActivity;
import com.example.servit.OrderDetailActivity;
import com.example.servit.R;
import com.example.servit.model.ExtraServiceModel;
import com.example.servit.model.OrderCategoryModel;
import com.example.servit.model.OrderModel;
import com.example.servit.model.ServiceModel;
import com.example.servit.model.eventModel;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;


public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.orderViewHolder> {

    private Context mContext;
    private static ArrayList<OrderModel> orderModels;
    private static ArrayList<OrderCategoryModel> orderCategoryModels;
    private static ArrayList<ExtraServiceModel> serviceModels;
    private static OrderAdapter instance;
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();
//    protected SwipeItemRecyclerMangerImpl mItemManger = new SwipeItemRecyclerMangerImpl(this);


    public OrderAdapter(Context mContext, ArrayList<OrderModel> orderModels) {
        this.mContext = mContext;
        this.orderModels = orderModels;
    }

    private OrderAdapter() {
        if (null == orderModels) {
            orderModels = new ArrayList<>();
        }
    }

    @NonNull
    @Override
    public orderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_card, parent, false);
        return new OrderAdapter.orderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull orderViewHolder holder, int position) {

        final OrderModel orderModel = orderModels.get(position);

//        viewBinderHelper.setOpenOnlyOne(true);
//        viewBinderHelper.bind(holder.parent, orderModel.getUserID());
//        viewBinderHelper.closeLayout(orderModel.getOrderDate());
        holder.serviceName.setText(orderModel.getCatName() + " "+ orderModel.getServiceName());
        holder.extraName.setText(orderModel.getExtraName());
        holder.orderTime.setText(orderModel.getOrderTime());
        holder.orderDate.setText(orderModel.getOrderDate());

//        mItemManger.bind(holder.itemView, position);


        if (orderModel.getStatus() == 1) {
            holder.constraintLayout.setBackground(mContext.getDrawable(R.drawable.order_bg_accept));
        } else if (orderModel.getStatus() == 2) {
            holder.constraintLayout.setBackground(mContext.getDrawable(R.drawable.order_bg_cancel));
        } else if (orderModel.getStatus() == 3) {
            holder.constraintLayout.setBackground(mContext.getDrawable(R.drawable.order_bg_end));
        }



        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putInt("servicePrice", orderModel.getServicePrice());
                bundle.putInt("extraPrice", orderModel.getExtraPrice());
                bundle.putString("serviceName", orderModel.getServiceName());
//                bundle.putString("extraName", orderModel.getExtraName());
                bundle.putString("orderDate", orderModel.getOrderDate());
                bundle.putStringArrayList("extraName", orderModel.getExtraListName());
                bundle.putIntegerArrayList("extraListPrice", orderModel.getExtraListPrice());
                bundle.putString("orderTime", orderModel.getOrderTime());
                bundle.putString("userName", orderModel.getUserName());
                bundle.putString("userNote", orderModel.getUserNote());
                bundle.putString("userAddress", orderModel.getUserAddress());
                bundle.putString("userPhone", orderModel.getUserPhone());
                bundle.putInt("orderTotal", orderModel.getOrderTotal());
                Intent intent = new Intent(mContext, OrderDetailActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return orderModels.size();
    }

    public void setOrderModels(ArrayList<OrderModel> orderModels) {
        this.orderModels = orderModels;
        notifyDataSetChanged();
    }

    public static OrderAdapter getInstance() {
        if (null == instance) {
            instance = new OrderAdapter();
        }
        return instance;
    }

    public static ArrayList<OrderModel> getAllOrder() {
        return orderModels;
    }

    public class orderViewHolder extends RecyclerView.ViewHolder {

        private TextView serviceName, extraName, orderDate, orderTime;
        private MaterialCardView card;
        private ConstraintLayout constraintLayout;


        public orderViewHolder(@NonNull View itemView) {
            super(itemView);
//            parent = itemView.findViewById(R.id.parent);
            card = itemView.findViewById(R.id.card);
            serviceName = itemView.findViewById(R.id.txt_service_name);
            extraName = itemView.findViewById(R.id.extraName);
            orderDate = itemView.findViewById(R.id.orderDate);
            orderTime = itemView.findViewById(R.id.orderTime);
            constraintLayout = itemView.findViewById(R.id.order_layout);

        }
    }
}
