package com.example.servit.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.servit.PriceChangedListener;
import com.example.servit.R;
import com.example.servit.model.OrderCategoryModel;

import java.util.ArrayList;

public class ServiceOrderAdapter extends RecyclerView.Adapter<ServiceOrderAdapter.ServiceOrderViewHolder> {

    private int selectedPosition;

    private Context mContext;
    private static ArrayList<OrderCategoryModel> orderCategoryModels;
    private static ServiceOrderAdapter instance;
    private static RecyclerItemClickListener listener;



    public ServiceOrderAdapter(Context mContext, ArrayList<OrderCategoryModel> orderCategoryModels, RecyclerItemClickListener listener) {
        this.orderCategoryModels = orderCategoryModels;
        this.listener = listener;
        this.mContext = mContext;
    }

    private ServiceOrderAdapter(){
        if (null == orderCategoryModels){
            orderCategoryModels = new ArrayList<>();
        }
    }

    @NonNull
    @Override
    public ServiceOrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_card, parent, false);
            return new ServiceOrderAdapter.ServiceOrderViewHolder(view);
//  TAI BANGET ERROR NYA INFLATE GA BISA DI mCONTEXT GA TAU KENAPA AING BINGUNG
    }


    @Override
    public void onBindViewHolder(@NonNull ServiceOrderViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        final OrderCategoryModel orderCategoryModel = orderCategoryModels.get(position);
        holder.txt_service_name.setText(orderCategoryModel.getServiceName());



        if (orderCategoryModel != null){
            if (selectedPosition == position){
                holder.parent.setCardBackgroundColor(mContext.getResources().getColor(R.color.very_light_blue));
            }else {
                holder.parent.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
            }
        }

        holder.bind(orderCategoryModel, listener);

    }

    @Override
    public int getItemCount() {
        return orderCategoryModels.size();
    }

    public void setCatModels(ArrayList<OrderCategoryModel> orderCategoryModels){
        this.orderCategoryModels = orderCategoryModels;
        notifyDataSetChanged();
    }

    public static ServiceOrderAdapter getInstance(){
        if (null == instance){
            instance = new ServiceOrderAdapter();
        }
        return instance;
    }

    public static ArrayList<OrderCategoryModel> getAllOrder(){
        return orderCategoryModels;
    }

    public class ServiceOrderViewHolder extends RecyclerView.ViewHolder {

        private CardView parent;
        private TextView txt_service_name, txt_price_service;

        public ServiceOrderViewHolder(@NonNull View itemView) {
            super(itemView);

            parent = itemView.findViewById(R.id.parent);
            txt_service_name = itemView.findViewById(R.id.txt_service_name);
        }

        public void bind(final OrderCategoryModel orderCategoryModel, final RecyclerItemClickListener listener) {


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClickListener(orderCategoryModel, getLayoutPosition());

                }
            });

        }
    }

    public interface RecyclerItemClickListener {

        void onClickListener(OrderCategoryModel orderCategoryModel, int position);


    }



    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

}
