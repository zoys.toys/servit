package com.example.servit.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Selection;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.servit.R;
import com.example.servit.model.ExtraServiceModel;
import com.example.servit.model.OrderCategoryModel;

import java.util.ArrayList;

public class ExtraServiceAdapter extends RecyclerView.Adapter<ExtraServiceAdapter.ExtraServiceViewModel> {

    private int selectedPosition;
    private SparseBooleanArray storeChecked = new SparseBooleanArray();


    Context mContext;
    private static ArrayList<ExtraServiceModel> serviceModels;
    private static ExtraServiceAdapter instance;
    private RecyclerItemClickListener listener;


    public ExtraServiceAdapter(Context mContext, ArrayList<ExtraServiceModel> serviceModels, RecyclerItemClickListener listener) {
        this.serviceModels = serviceModels;
        this.mContext = mContext;
        this.listener = listener;
    }

    private ExtraServiceAdapter() {
        if (null == serviceModels) {
            serviceModels = new ArrayList<>();
        }
    }


    @NonNull
    @Override
    public ExtraServiceViewModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.extra_service_card, parent, false);
        return new ExtraServiceAdapter.ExtraServiceViewModel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ExtraServiceViewModel holder, @SuppressLint("RecyclerView") int position) {


        final ExtraServiceModel extraServiceModel = serviceModels.get(position);
        holder.txt_extra_service_name.setText(extraServiceModel.getExtraServiceName());
        Glide.with(mContext).load(extraServiceModel.getImgUrl()).into(holder.extra_image);

        if (extraServiceModel != null) {

//            holder.parent.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));

            holder.parent.setCardBackgroundColor(extraServiceModel.isSelected() ? mContext.getColor(R.color.very_light_blue) : Color.WHITE);


//            if (selectedPosition == position){
//                holder.parent.setCardBackgroundColor(mContext.getResources().getColor(R.color.very_light_blue));
//                holder.parent.setCardBackgroundColor(extraServiceModel.isSelected() ? Color.CYAN : Color.WHITE);
//            }else {
//                holder.parent.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
//            }

//            if (extraServiceModel.isSelected()) {
//                holder.parent.setCardBackgroundColor(mContext.getResources().getColor(R.color.very_light_blue));
//            }
//            else {
//                holder.parent.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
//            }
        }

        holder.bind(extraServiceModel, listener, holder);
    }

    @Override
    public int getItemCount() {
        return serviceModels.size();
    }

    public void setExtraModels(ArrayList<ExtraServiceModel> extraServiceModels) {
        this.serviceModels = extraServiceModels;
        notifyDataSetChanged();
    }

    public static ExtraServiceAdapter getInstance() {
        if (null == instance) {
            instance = new ExtraServiceAdapter();
        }
        return instance;
    }


    public static ArrayList<ExtraServiceModel> getAllExtra() {
        return serviceModels;
    }


    public class ExtraServiceViewModel extends RecyclerView.ViewHolder {

        private CardView parent;
        private TextView txt_extra_service_name, txt_price_extra;
        private ImageView extra_image;


        public ExtraServiceViewModel(@NonNull View itemView) {
            super(itemView);

            parent = itemView.findViewById(R.id.parent);
            txt_extra_service_name = itemView.findViewById(R.id.txt_extra_name);
            extra_image = itemView.findViewById(R.id.extra_service_img);
            txt_price_extra = itemView.findViewById(R.id.price_extra);
        }


        public void bind(final ExtraServiceModel extraServiceModel, final RecyclerItemClickListener listener, ExtraServiceViewModel holder) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    extraServiceModel.setSelected(!extraServiceModel.isSelected());
//
                    holder.parent.setCardBackgroundColor(extraServiceModel.isSelected() ? mContext.getColor(R.color.very_light_blue) : Color.WHITE);


//                    ExtraServiceModel extraServiceModel = serviceModels.get(position);
//                    if (extraServiceModel.isSelected()){
//                        extraServiceModel.setSelected(false);
//                    }else {
//                        extraServiceModel.setSelected(true);
//                    }
                    listener.onClickListener(extraServiceModel, getAdapterPosition());
//
//                    serviceModels.set()


                }
            });
        }
    }

    public interface RecyclerItemClickListener {
        void onClickListener(ExtraServiceModel extraServiceModel, int position);
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

}
