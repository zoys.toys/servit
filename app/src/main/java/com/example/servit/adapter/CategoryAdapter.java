package com.example.servit.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.servit.OrderActivity;
import com.example.servit.OrderActivity2;
import com.example.servit.R;
import com.example.servit.model.CatModel;

import java.util.ArrayList;
import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private Context mContext;
    private static ArrayList<CatModel> CatModels;
    private static CategoryAdapter instance;

    public CategoryAdapter(Context mContext, ArrayList<CatModel> CatModels) {
        this.mContext = mContext;
        this.CatModels = CatModels;
    }

    private CategoryAdapter(){
        if (null == CatModels){
            CatModels = new ArrayList<>();
        }
    }

    @NonNull
    @Override
    public CategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.cat_card, parent, false);
        return new CategoryAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final CatModel catModel = CatModels.get(position);
        holder.txtCatName.setText(catModel.getCatName());

        Glide.with(mContext).load(catModel.getIconUrl()).into(holder.iconUrl);

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, OrderActivity.class);
                intent.putExtra("catName", catModel.getCatName());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return CatModels.size();
    }

    public void setCatModels(ArrayList<CatModel> catModels){
        this.CatModels = catModels;
        notifyDataSetChanged();
    }

    public static CategoryAdapter getInstance() {
        if (null == instance) {
            instance = new CategoryAdapter();
        }
        return instance;
    }

    public static ArrayList<CatModel> getAllCat() {
        return CatModels;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{

        private CardView parent;
        private ImageView iconUrl;
        private TextView txtCatName;

        public MyViewHolder(@NonNull View view){
            super(view);
            parent = view.findViewById(R.id.parent);
            txtCatName = view.findViewById(R.id.txtCatName);
            iconUrl = view.findViewById(R.id.iconCat);
        }

    }
}
