package com.example.servit.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.servit.EventDetailActivity;
import com.example.servit.R;
import com.example.servit.model.eventModel;

import java.util.ArrayList;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.eventViewHolder> {

    private Context mContext;
    private static ArrayList<eventModel> eventModels;
    private static EventAdapter instance;

    public EventAdapter(Context mContext, ArrayList<eventModel> eventModels) {
        this.mContext = mContext;
        this.eventModels = eventModels;
    }

    private EventAdapter() {
        if (null == eventModels) {
            eventModels = new ArrayList<>();
        }
    }

    @NonNull
    @Override
    public eventViewHolder
    onCreateViewHolder(@NonNull ViewGroup parent,
                       int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_card, parent, false);
        return new EventAdapter.eventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull eventViewHolder holder, int position) {

        final eventModel eventModel = eventModels.get(position);
        holder.txt_eventName.setText(eventModel.getEventName());

        Glide.with(mContext).load(eventModel.getImageUrl()).into(holder.event_image);

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Bundle bundle = new Bundle();
                bundle.putString("eventName", eventModel.getEventName());
                bundle.putString("imageUrl", eventModel.getImageUrl());
                bundle.putString("longDesc", eventModel.getLongDesc());
                bundle.putString("title", eventModel.getTitle());
                bundle.putString("id", eventModel.getId());
                Intent intent = new Intent(mContext, EventDetailActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return eventModels.size();
    }

    public void setEventModels(ArrayList<eventModel> eventModels) {
        this.eventModels = eventModels;
        notifyDataSetChanged();
    }

    public static EventAdapter getInstance() {
        if (null == instance) {
            instance = new EventAdapter();
        }
        return instance;
    }


    public static ArrayList<eventModel> getAllEvent() {
        return eventModels;
    }

    public class eventViewHolder extends RecyclerView.ViewHolder {

        TextView txt_eventName;
        ImageView event_image;
        CardView parent;

        public eventViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_eventName = itemView.findViewById(R.id.txt_eventName);
            event_image = itemView.findViewById(R.id.event_image);
            parent = itemView.findViewById(R.id.parent);
        }
    }
}
