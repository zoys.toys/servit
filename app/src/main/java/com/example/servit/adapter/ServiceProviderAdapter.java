package com.example.servit.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.servit.R;
import com.example.servit.ServiceProviderDetailActivity;
import com.example.servit.model.OrderCategoryModel;
import com.example.servit.model.ServiceModel;

import java.util.ArrayList;
import java.util.Locale;

public class ServiceProviderAdapter extends RecyclerView.Adapter<ServiceProviderAdapter.ServiceProviderViewHolder> implements Filterable {

    private Context mContext;
    private static ArrayList<ServiceModel> serviceModels;
    private ArrayList<ServiceModel> serviceModelsFiltered;
    private static ServiceProviderAdapter instance;
//    private SelectedProvider selectedProvider;

    public ServiceProviderAdapter(Context mContext, ArrayList<ServiceModel> serviceModels) {
        this.mContext = mContext;
        this.serviceModels = serviceModels;
//        this.selectedProvider = selectedProvider;
        this.serviceModelsFiltered = serviceModels;
    }

    private ServiceProviderAdapter(){
        if (null == serviceModels){
            serviceModels = new ArrayList<>();
        }
    }

    public void filterList(ArrayList<ServiceModel> filterlist){

        serviceModels = filterlist;

        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ServiceProviderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.provider_list_card, parent, false);
        return new ServiceProviderAdapter.ServiceProviderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceProviderViewHolder holder, int position) {

        final ServiceModel serviceModel = serviceModels.get(position);
        holder.provider_name_txt.setText(serviceModel.getServiceName());
        holder.short_desc_txt.setText(serviceModel.getServiceBriefDesc());
        Glide.with(mContext).load(serviceModel.getServiceUrl()).into(holder.img_provider);

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("serviceName", serviceModel.getServiceName());
                bundle.putString("serviceAddress", serviceModel.getServiceAddress());
                bundle.putString("serviceProvide", serviceModel.getServiceProvide());
                bundle.putString("serviceUrl", serviceModel.getServiceUrl());
                bundle.putString("serviceLongDesc", serviceModel.getServiceLongDesc());
                Intent intent = new Intent(mContext, ServiceProviderDetailActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return serviceModels.size();
    }

    public void setProviderModels(ArrayList<ServiceModel> serviceModels){
        this.serviceModels = serviceModels;
        notifyDataSetChanged();
    }

    public static ServiceProviderAdapter getInstance(){
        if (null == instance){
            instance = new ServiceProviderAdapter();
        }
        return instance;
    }

//    public interface SelectedProvider{
//        void selected(ServiceModel serviceModel);
//    }
//
//    public interface onClickLisneter{
//        public void onClick(View v, int position);
//    }


    public static ArrayList<ServiceModel> getAllProvider(){
        return serviceModels;
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();

                if(constraint == null || constraint.length() == 0){
                    filterResults.count = serviceModelsFiltered.size();
                    filterResults.values = serviceModelsFiltered;
                }else{
                    String searchchar = constraint.toString().toLowerCase(Locale.ROOT);

                    ArrayList<ServiceModel> resultData = new ArrayList<>();

                    for (ServiceModel serviceModel: serviceModelsFiltered){
                        if (serviceModel.getServiceName().toLowerCase(Locale.ROOT).contains(searchchar)){
                            resultData.add(serviceModel);
                        }
                    }
                    filterResults.count = resultData.size();
                    filterResults.values = resultData;
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {


                serviceModels = (ArrayList<ServiceModel>) results.values;
                notifyDataSetChanged();
            }
        };

        return filter;
    }

    public class ServiceProviderViewHolder extends RecyclerView.ViewHolder {

        private CardView parent;
        private TextView provider_name_txt, short_desc_txt;
        private ImageView img_provider;

        public ServiceProviderViewHolder(@NonNull View itemView) {
            super(itemView);

            parent = itemView.findViewById(R.id.parent);
            provider_name_txt = itemView.findViewById(R.id.provider_name_txt);
            short_desc_txt = itemView.findViewById(R.id.short_desc_txt);
            img_provider = itemView.findViewById(R.id.img_provider);

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
////                    selectedProvider.selected(serviceModels.get(getLayoutPosition()));
//                }
//            });
        }
    }
}
