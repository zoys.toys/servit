package com.example.servit.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.servit.R;
import com.example.servit.ServiceProviderDetailActivity;
import com.example.servit.model.ServiceModel;

import java.util.ArrayList;
import java.util.List;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceViewHolder> {

    private Context mContext;
    private static ArrayList<ServiceModel> serviceModels;
    private static ServiceAdapter instance;

    public ServiceAdapter(Context mContext, ArrayList<ServiceModel> serviceModels) {
        this.mContext = mContext;
        this.serviceModels = serviceModels;
    }


    private ServiceAdapter(){
        if (null == serviceModels){
            serviceModels = new ArrayList<>();
        }
    }


    @NonNull
    @Override
    public ServiceAdapter.ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.favorite_card, parent, false);
        return new ServiceAdapter.ServiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceViewHolder holder, int position) {

        final ServiceModel serviceModel = serviceModels.get(position);
        Glide.with(mContext).load(serviceModel.getServiceUrl()).into(holder.favorite_img);

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("serviceName", serviceModel.getServiceName());
                bundle.putString("serviceAddress", serviceModel.getServiceAddress());
                bundle.putString("serviceProvide", serviceModel.getServiceProvide());
                bundle.putString("serviceUrl", serviceModel.getServiceUrl());
                bundle.putString("serviceLongDesc", serviceModel.getServiceLongDesc());
                Intent intent = new Intent(mContext, ServiceProviderDetailActivity.class);
                intent.putExtras(bundle);
                mContext.startActivity(intent);

            }
        });




    }

    @Override
    public int getItemCount() {
        return serviceModels.size();
    }

    public void setServiceModels(ArrayList<ServiceModel> serviceModels){
        this.serviceModels = serviceModels;
        notifyDataSetChanged();
    }

    public static ServiceAdapter getInstance(){
        if (null == instance){
            instance = new ServiceAdapter();
        }
        return instance;
    }

    public static ArrayList<ServiceModel> getAllService(){
        return serviceModels;
    }

    public class ServiceViewHolder extends RecyclerView.ViewHolder{

        ImageView favorite_img;
        CardView parent;

        public ServiceViewHolder(@NonNull View itemView) {
            super(itemView);

            favorite_img = itemView.findViewById(R.id.imageFav);
            parent = itemView.findViewById(R.id.parent);
        }
    }
}
