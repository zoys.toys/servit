package com.example.servit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.servit.databinding.ActivityConfirmOrderBinding;
import com.example.servit.databinding.ActivityOrderUserInfoBinding;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ConfirmOrderActivity extends AppCompatActivity {

    TextView txtUserName, txtUserPhone, txtUserAddress, txtOrderDate, txtOrderTime, txtServiceName, txtExtraName, txtServicePrice, txtExtraPrice, txtTotalPrice, txtOrderTotal, txtUserNote, txtToolbar;
    ImageView nextBtn;
    String userId, userName, userAddress, orderDate, orderTime, serviceName, extraName, userNote1, userPhone, catName;
    FirebaseUser user;
    int servicePrice, extraPrice, totalPrice,  orderTotal;
    ArrayList<String> list;
    ArrayList<Integer> list2;
    FirebaseAuth fauth;
    FirebaseDatabase fdata;
    DatabaseReference mDatabase;
    ActivityConfirmOrderBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityConfirmOrderBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        MaterialToolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtToolbar = findViewById(R.id.txt_name);
        txtToolbar.setText("Confirm Order");

        fauth = FirebaseAuth.getInstance();
        fdata = FirebaseDatabase.getInstance();
        user = fauth.getCurrentUser();
        mDatabase = fdata.getReference("orders");
        userId = user.getUid();
        list = new ArrayList<>();
        list2 = new ArrayList<>();

        txtUserName = binding.txtName1;
        txtUserAddress = binding.txtAddress;
        txtUserPhone = binding.txtPhone;
        txtUserNote = binding.userNote;
        txtOrderDate = binding.txtDate;
        txtOrderTime = binding.txtTime;
        txtOrderTotal = binding.txtTotalUang;
        txtTotalPrice = binding.totalPrice;
        txtExtraName = binding.extraName;
        txtExtraPrice = binding.extraPrice;
        txtServicePrice = binding.servicePrice;
        txtServiceName = binding.serviceName;

        setAndgetData();

        nextBtn = binding.imgBtnNext;

        addDataOrder();


    }

    private void setAndgetData() {

        orderTotal = getIntent().getExtras().getInt("orderTotal");

        catName = getIntent().getExtras().getString("catName");
        userNote1 = getIntent().getExtras().getString("userNote");
        userName = getIntent().getExtras().getString("userName");
        userPhone = getIntent().getExtras().getString("userPhone");
        userAddress = getIntent().getExtras().getString("userAddress");
        orderDate = getIntent().getExtras().getString("orderDate");
        orderTime = getIntent().getExtras().getString("orderTime");
        serviceName = getIntent().getExtras().getString("serviceName");
        list = getIntent().getExtras().getStringArrayList("extraName");
        list2 = getIntent().getExtras().getIntegerArrayList("extraListPrice");
        servicePrice = getIntent().getExtras().getInt("servicePrice");
        extraPrice = getIntent().getExtras().getInt("extraPrice");

        Log.d("ConfirmOrderActivity", String.valueOf(list));


        txtUserName.setText(userName);
        txtUserAddress.setText(userAddress);
        txtUserPhone.setText(userPhone);
        txtUserNote.setText(userNote1);
        txtOrderDate.setText(orderDate);
        txtOrderTime.setText(orderTime);
        txtOrderTotal.setText("Rp. " + String.valueOf(orderTotal));
        txtTotalPrice.setText("Rp. " + String.valueOf(orderTotal));
//        txtExtraName.setText(extraName);

        txtExtraName.setText("");

        for (int i = 0; i < list.size(); i++){
            txtExtraName.append(list.get(i));
            txtExtraName.append("\n\n");
        }

//        txtExtraPrice.setText("Rp. " + String.valueOf(extraPrice));

        txtExtraPrice.setText("");

        for (int i = 0; i < list2.size(); i++){
            txtExtraPrice.append("Rp. "+list2.get(i).toString());
            txtExtraPrice.append("\n\n");
        }

        txtServicePrice.setText("Rp. " + String.valueOf(servicePrice));
        txtServiceName.setText(serviceName);

        Log.d("dsadas113131", userNote1);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                Intent intent = new Intent(this, OrderUserInfoActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
//                finish();
//                moveTaskToBack(true);
        }

        return super.onOptionsItemSelected(item);
    }

    private void addDataOrder(){

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> added = new HashMap<>();
                added.put("catName", catName);
                added.put("userName", userName);
                added.put("userAddress", userAddress);
                added.put("userPhone", userPhone);
                added.put("userNote", userNote1);
                added.put("orderDate", orderDate);
                added.put("orderTime", orderTime);
                added.put("orderTotal", orderTotal);
                added.put("extraName", extraName);
                added.put("serviceName", serviceName);
                added.put("userID", userId);
                added.put("extraListName",list);
                added.put("extraListPrice", list2);
                added.put("status", 0);
                added.put("servicePrice", servicePrice);
                added.put("extraPrice", extraPrice);

                String id = mDatabase.push().getKey();
                added.put("orderId", id);

                mDatabase.child(id).setValue(added).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        Toast.makeText(ConfirmOrderActivity.this, "Order Successfully added", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ConfirmOrderActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
            }
        });
    }
}