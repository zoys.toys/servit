package com.example.servit;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.servit.model.UserAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;


public class LoginTabFragment extends Fragment {

    View objectLoginFragment;
    EditText userEmail, userPassword;
    MaterialButton materialLoginButton;
    String userID;
    DatabaseReference databaseUser;
    private FirebaseAuth fAuth;
    private FirebaseDatabase fdata;
    ProgressBar progressBar;
    private static final String TAG = "Login";
    private TextView txtWarnEmail, txtWarnPass, txtForgot;
    List<UserAccount> users;
    GoogleSignInAccount googleSignInAccount;
    

    public LoginTabFragment() {

        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        FirebaseUser currentUser = fAuth.getCurrentUser();
//
//        if (currentUser != null) {
//            Intent intent = new Intent(getContext(), MainActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
////            Toast.makeText(this, "Account Created Successfuly" + userID, Toast.LENGTH_SHORT).show();
//
//        } else {
//            Log.w(TAG, "Aneh");
//
//        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        objectLoginFragment = inflater.inflate(R.layout.login_tab_fragment, container, false);

        initElement();

        return objectLoginFragment;
    }

    private void initElement() {

        try {

            Log.d(TAG, "initElement: Started");

            initView();

            txtForgot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getContext(), ForgotPassActivity.class));
                }
            });

            materialLoginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Log.v(TAG, Boolean.toString(validating()));
                    if (validating()) {
//
                        userLogin();

                    }
                }
            });

        } catch (Exception e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void initView() {
        userEmail = objectLoginFragment.findViewById(R.id.email);
        userPassword = objectLoginFragment.findViewById(R.id.password1);
        materialLoginButton = objectLoginFragment.findViewById(R.id.loginButton);

        txtWarnEmail = objectLoginFragment.findViewById(R.id.txtWarnEmail);
        txtWarnPass = objectLoginFragment.findViewById(R.id.txtWarnPass);
        txtForgot = objectLoginFragment.findViewById(R.id.txt_forgot);

        fAuth = FirebaseAuth.getInstance();
        fdata = FirebaseDatabase.getInstance();
        users = new ArrayList<>();

    }

    private boolean validating() {
        Log.d(TAG, "validateData: Started");

        String email = userEmail.getText().toString().trim();
        String password = userPassword.getText().toString().trim();


        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {

            txtWarnEmail.setVisibility(View.VISIBLE);
            txtWarnEmail.setText("Enter your Email");
            txtWarnPass.setVisibility(View.VISIBLE);
            txtWarnPass.setText("Enter your Password");

            userEmail.setBackgroundResource(R.drawable.edit_text_false_bg);
            userPassword.setBackgroundResource(R.drawable.edit_text_false_bg);

            if (!TextUtils.isEmpty(email)) {
                txtWarnEmail.setVisibility(View.GONE);
                txtWarnEmail.setText("Enter your Email");
                userEmail.setBackgroundResource(R.drawable.edit_text_bg);
                return false;

            }
            if (!TextUtils.isEmpty(password)) {
                txtWarnPass.setVisibility(View.GONE);
                txtWarnPass.setText("Enter your Password");
                userPassword.setBackgroundResource(R.drawable.edit_text_bg);
                return false;
            }

        }
        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
            userEmail.setBackgroundResource(R.drawable.edit_text_bg);
            userPassword.setBackgroundResource(R.drawable.edit_text_bg);
            txtWarnEmail.setVisibility(View.GONE);
            txtWarnPass.setVisibility(View.GONE);

//            if (!TextUtils.isEmpty(userEmail.getText())) {
//                txtWarnEmail.setVisibility(View.GONE);
//                userEmail.setBackgroundResource(R.drawable.edit_text_bg);
//                return false;
//            }
//            else if (!TextUtils.isEmpty(userPassword.getText())) {
//                txtWarnPass.setVisibility(View.GONE);
//                userPassword.setBackgroundResource(R.drawable.edit_text_bg);
//                return false;
//            }
            if (!validateUserEmail(userEmail.getText().toString())) {
                txtWarnEmail.setVisibility(View.VISIBLE);
                userEmail.setBackgroundResource(R.drawable.edit_text_false_bg);
                txtWarnEmail.setText("Email not valid!");
                return false;

            } else if (userPassword.length() <= 3) {
                txtWarnPass.setVisibility(View.VISIBLE);
                userPassword.setBackgroundResource(R.drawable.edit_text_false_bg);
                txtWarnPass.setText("Password is too short");
                return false;

            } else {
                return true;
            }

        }

        return false;
    }

    private void userLogin() {

        String email = userEmail.getText().toString().trim();
        String password = userPassword.getText().toString().trim();

        fAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "signInWithEmail: success");
                    FirebaseUser user = fAuth.getCurrentUser();
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
////                    Log.w(TAG, "signInWithEmail: fail", task.getException());
//                    Toast.makeText(getContext(), "Error ! " + , Toast.LENGTH_SHORT).show();
//                    Toast.makeText(getContext(), "Error ! " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
//               progressBar.setVisibility(View.GONE);

//                    Log.w(TAG, "signInwithEmail: " + )
                    if (task.getException() instanceof FirebaseAuthInvalidUserException) {
                        txtWarnEmail.setVisibility(View.VISIBLE);
                        userEmail.setBackgroundResource(R.drawable.edit_text_false_bg);
                        txtWarnEmail.setText("Email doesnt exist");
                        userEmail.requestFocus();
                        Toast.makeText(getContext(), "User with this email doesnt exist.", Toast.LENGTH_SHORT).show();
                    }else if(task.getException() instanceof FirebaseAuthInvalidCredentialsException){
                        txtWarnPass.setVisibility(View.VISIBLE);
                        userPassword.setBackgroundResource(R.drawable.edit_text_false_bg);
                        txtWarnPass.setText("Wrong password");
                        userPassword.requestFocus();
                        Toast.makeText(getContext(), "Wrong password", Toast.LENGTH_SHORT).show();
                    }
                    Log.w(TAG, "signInWithEmail: fail", task.getException());
                }
            }
        });

    }

    private static boolean validateUserEmail(String email) {
        return email.contains("@") && email.contains(".");
    }

}