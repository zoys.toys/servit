package com.example.servit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.servit.adapter.OrderAdapter;
import com.example.servit.databinding.OrderFragmentBinding;
import com.example.servit.helper.MyButtonClickLisneter;
import com.example.servit.helper.SwipeHelper;
import com.example.servit.model.OrderModel;
import com.example.servit.viewModels.OrderViewModel;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderFragment extends Fragment {

    private OrderViewModel mViewModel;
    private OrderFragmentBinding binding;
    OrderAdapter orderAdapter;
    RecyclerView recyclerView;
    private ArrayList<OrderModel> orderModels;
    DatabaseReference mDatabase;
    FirebaseAuth fauth;
    FirebaseDatabase fdata;
    FirebaseUser user;
    private String userId, txtReason;
    MaterialToolbar toolbar;
    Context context;

//    public interface moveItemListener {
//        void sendValue(String value2);
//
//    }

//    public OrderFragment(Context context,moveItemListener listener){
//        this.context = context;
//        this.listener = listener;
//
//    }

    public OrderFragment() {

    }

//    public moveItemListener listener;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

//        mViewModel = new ViewModelProvider(this).get(OrderViewModel.class);
//
        binding = OrderFragmentBinding.inflate(inflater, container, false);
//
        View root = binding.getRoot();

        toolbar = binding.toolbarLayout;

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        getActivity().setTitle("Order History");

        fauth = FirebaseAuth.getInstance();
        fdata = FirebaseDatabase.getInstance();
        user = fauth.getCurrentUser();
        mDatabase = fdata.getReference("orders");
        orderModels = new ArrayList<>();
        userId = user.getUid();
//
//        final TextView textView = binding.orderTxt;
//        mViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);

        recyclerView = binding.recyclesOrder;

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                orderModels.clear();

                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    OrderModel orderModel = dataSnapshot.getValue(OrderModel.class);

                    if (userId.equals(orderModel.getUserID())) {
                        orderModels.add(orderModel);
                    }
                }
                orderAdapter = new OrderAdapter(getContext(), orderModels);
                recyclerView.setAdapter(orderAdapter);
                orderAdapter.setOrderModels(OrderAdapter.getInstance().getAllOrder());


                SwipeHelper swipeHelper = new SwipeHelper(getContext(), recyclerView, 200) {
                    @Override
                    public void instantiateMyButton(RecyclerView.ViewHolder viewHolder, List<SwipeHelper.MyButton> buffer) {
                        buffer.add(new MyButton(getContext(), "Cancel", 30, 0,
                                Color.parseColor("#FF3c30"),
                                pos -> {


                                    final OrderModel orderModel = orderModels.get(pos);

//                                        Map<String, Object> value = new HashMap<>();
//                                        value.put("status", 3);
//
                                    String orderID = orderModel.getOrderId();

                                    cancelDialog(orderID);
//                                        mDatabase.child(orderModel.getOrderId()).updateChildren(value);

//                                    Toast.makeText(getContext(), "Order Canceled" + orderID, Toast.LENGTH_SHORT).show();

//                                    listener.sendValue(orderID);
                                }));
                    }
                };


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        return root;
    }


    public void cancelDialog(String value) {
        DialogFragment dialogFragment = new CancelDialog();
        Bundle b = new Bundle();
        b.putString("orderId", value);

        dialogFragment.setArguments(b);
        dialogFragment.show(requireActivity().getSupportFragmentManager(), "CancelDialog");
    }


//    @Override
//    public void onDialogPositiveClick(String dialogFragment) {
//
//        Log.d("Order Fragment", dialogFragment);
//
//        txtReason = dialogFragment;
//    }


}