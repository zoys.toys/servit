package com.example.servit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.example.servit.adapter.ServiceProviderAdapter;
import com.example.servit.databinding.ProviderListFragmentBinding;
import com.example.servit.model.ServiceModel;
import com.example.servit.viewModels.ProviderListViewModel;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Locale;

public class ProviderListFragment extends Fragment {

    private ProviderListViewModel mViewModel;
    private TextView textView;
    private ProviderListFragmentBinding binding;
    MaterialToolbar toolbar;
    RecyclerView recyclerView;
    ServiceProviderAdapter serviceProviderAdapter;
    private ArrayList<ServiceModel> serviceModels;
    FirebaseDatabase fData;
    DatabaseReference mServiceDatabase;

    public ProviderListFragment(){

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = ProviderListFragmentBinding.inflate(inflater, container, false);
        mViewModel = new ViewModelProvider(this).get(ProviderListViewModel.class);
        View root = binding.getRoot();
        fData = FirebaseDatabase.getInstance();
        mServiceDatabase = fData.getReference("provider");
        serviceModels = new ArrayList<>();


        recyclerView = binding.recyclerviewProvider;
        toolbar = binding.toolbarLayout;

        recyclerView.setHasFixedSize(true);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        getActivity().setTitle("Service Provider");
        setHasOptionsMenu(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));



        setDataList();
//        serviceProviderAdapter = new ServiceProviderAdapter(getContext(), serviceModels, this);

//        textView = binding.txtProvider;
//        mViewModel.getText().observe(getViewLifecycleOwner(), s -> textView.setText(s));

        return root;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();

        inflater.inflate(R.menu.toolbar_provider_menu,menu);
        MenuItem item = menu.findItem(R.id.search_icon);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItem.SHOW_AS_ACTION_IF_ROOM);

        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                serviceProviderAdapter.getFilter().filter(newText);

                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.search_icon) {
                return true;
        }

        return super.onOptionsItemSelected(item);
    }



    private void setDataList(){

        mServiceDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()){
                    ServiceModel serviceModel = dataSnapshot.getValue(ServiceModel.class);
                    serviceModels.add(serviceModel);
                }

                serviceProviderAdapter = new ServiceProviderAdapter(getContext(), serviceModels);
                recyclerView.setAdapter(serviceProviderAdapter);
                serviceProviderAdapter.setProviderModels(ServiceProviderAdapter.getInstance().getAllProvider());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w("Providerrr", "helooooooooooooo" + error.getMessage());
            }
        });

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding=null;
    }



}