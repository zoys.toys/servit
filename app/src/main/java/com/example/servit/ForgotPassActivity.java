package com.example.servit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.servit.databinding.ActivityForgotPassBinding;
import com.example.servit.databinding.ActivityUserProfileBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotPassActivity extends AppCompatActivity {

    EditText emailEdit;
    FirebaseAuth fauth;
    MaterialButton resetBtn;
    ActivityForgotPassBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityForgotPassBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        emailEdit = binding.editEmail;
        fauth = FirebaseAuth.getInstance();
        resetBtn = binding.forgotBtn;

        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = emailEdit.getText().toString();

                if (TextUtils.isEmpty(email)){
                    Toast.makeText(ForgotPassActivity.this, "Please entered your registered Email", Toast.LENGTH_SHORT).show();
                    emailEdit.setError("Email is required");
                    emailEdit.requestFocus();
                }else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    Toast.makeText(ForgotPassActivity.this, "Required valid Email", Toast.LENGTH_SHORT).show();
                    emailEdit.setError("Invalid Email");
                    emailEdit.requestFocus();
                }else {
                    resetPassword(email);
                }
            }
        });
    }

    private void resetPassword(String email) {
        fauth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(ForgotPassActivity.this, "Please check your email inbox for reset password link", Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(getApplicationContext(), ViewPageLogRegis.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }else {
                    Toast.makeText(ForgotPassActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}