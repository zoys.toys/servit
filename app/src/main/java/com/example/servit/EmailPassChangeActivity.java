package com.example.servit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.servit.databinding.ActivityEmailPassChangeBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;

import java.util.regex.Pattern;

public class EmailPassChangeActivity extends AppCompatActivity {

    ActivityEmailPassChangeBinding binding;
    FirebaseAuth fauth;
    DatabaseReference mDatabase;
    FirebaseDatabase fdata;
    StorageReference sStorage;
    FirebaseUser user;
    String userId, emailR, passR, emailN, passN, passNR;
    EditText emailP, passwordR, passwordRR, passwordN, emailO;
    MaterialButton authenBtn, editSaveBtn, editSaveBtnP;
    TextView check, textTool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEmailPassChangeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        MaterialToolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        passwordN = binding.editPasswordReal;
        emailP = binding.editEmailNew;
        passwordR = binding.editPassNew;
        passwordRR = binding.editPassReNew;
        authenBtn = binding.btnAuthen;
        editSaveBtn = binding.updateCredential;
        check = binding.check;
        emailO = binding.editEmailOld;
        editSaveBtnP = binding.updateCredentialPass;
        textTool = binding.toolbar.txtName;

        editSaveBtnP.setEnabled(false);
        editSaveBtn.setEnabled(false);
        emailP.setEnabled(false);
        passwordR.setEnabled(false);
        passwordRR.setEnabled(false);

        textTool.setText("Change Email / Password");

        fauth = FirebaseAuth.getInstance();
        fdata = FirebaseDatabase.getInstance();
        user = fauth.getCurrentUser();
        userId = user.getUid();

        emailR = user.getEmail();
        emailO.setText(emailR);


        if (user.equals("")) {
            Toast.makeText(this, "No user detected", Toast.LENGTH_SHORT).show();
        } else {
            reAuthenticate(user);
        }

    }

    private void reAuthenticate(FirebaseUser user) {

        authenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                passR = passwordN.getText().toString();

                if (TextUtils.isEmpty(passR)) {
                    Toast.makeText(EmailPassChangeActivity.this, "Password is needed", Toast.LENGTH_SHORT).show();
                    passwordN.setError("Please enter your password");
                    passwordN.requestFocus();
                } else {

                    AuthCredential credential = EmailAuthProvider.getCredential(emailR, passR);

                    user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                {

                                    Toast.makeText(EmailPassChangeActivity.this, "User has been verified" + " You can update now", Toast.LENGTH_LONG).show();

                                    editSaveBtnP.setEnabled(true);
                                    editSaveBtn.setEnabled(true);
                                    emailP.setEnabled(true);
                                    passwordR.setEnabled(true);
                                    passwordN.setEnabled(false);
                                    passwordRR.setEnabled(true);
                                    authenBtn.setEnabled(false);
                                    check.setVisibility(View.VISIBLE);


                                    editSaveBtn.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.blue));

                                    editSaveBtnP.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            passN = passwordR.getText().toString();
                                            passNR = passwordRR.getText().toString();
                                            if (TextUtils.isEmpty(passN)) {
                                                Toast.makeText(EmailPassChangeActivity.this, "New Password is required", Toast.LENGTH_SHORT).show();
                                                passwordR.setError("Please enter new Password");
                                                passwordR.requestFocus();
                                            } else if (passN.length() >= 4) {
                                                Toast.makeText(EmailPassChangeActivity.this, "New Password length required more than 4", Toast.LENGTH_SHORT).show();
                                                passwordR.setError("Please enter solid new Password");
                                                passwordR.requestFocus();
                                            } else if (passN.matches(passR)) {
                                                Toast.makeText(EmailPassChangeActivity.this, "New Password cannot be same", Toast.LENGTH_SHORT).show();
                                                passwordR.setError("Please enter new Password");
                                                passwordR.requestFocus();
                                            } else if (!passN.matches(passNR)) {
                                                Toast.makeText(EmailPassChangeActivity.this, "New Password Confirm has to be match", Toast.LENGTH_SHORT).show();
                                                passwordR.setError("Please enter Correct same Password");
                                                passwordR.requestFocus();
                                            }else if (!passNR.matches(passN)) {
                                                Toast.makeText(EmailPassChangeActivity.this, "New Password Confirm has to be match", Toast.LENGTH_SHORT).show();
                                                passwordRR.setError("Please enter Correct same Password");
                                                passwordRR.requestFocus();
                                            }
                                            else {
                                                updatePassword(user);
                                            }
                                        }
                                    });
                                    editSaveBtn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            emailN = emailP.getText().toString();
                                            if (TextUtils.isEmpty(emailN)) {
                                                Toast.makeText(EmailPassChangeActivity.this, "New Email is required", Toast.LENGTH_SHORT).show();
                                                emailP.setError("Please enter new Email");
                                                emailP.requestFocus();
                                            } else if (!Patterns.EMAIL_ADDRESS.matcher(emailN).matches()) {
                                                Toast.makeText(EmailPassChangeActivity.this, "Please enter valid Email", Toast.LENGTH_SHORT).show();
                                                emailP.setError("Please provided valid Email");
                                                emailP.requestFocus();
                                            } else if (emailR.matches(emailN)) {
                                                Toast.makeText(EmailPassChangeActivity.this, "New Email cannot be same", Toast.LENGTH_SHORT).show();
                                                emailP.setError("Please enter new Email");
                                                emailP.requestFocus();
                                            } else {
                                                updateEmail(user);
                                            }
                                        }
                                    });
                                }
                            } else {
                                try {
                                    throw task.getException();
                                } catch (Exception e) {
                                    Toast.makeText(EmailPassChangeActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    private void updateEmail(FirebaseUser user) {

        user.updateEmail(emailN).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                    mDatabase = fdata.getReference("users");

                    mDatabase.child(userId).child("email").setValue(emailN).toString();

                    Toast.makeText(EmailPassChangeActivity.this, "Email successfully change", Toast.LENGTH_SHORT).show();
//                    user.sendEmailVerification();
                    Intent intent = new Intent(getApplicationContext(), UserProfileActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    try {
                        throw task.getException();
                    } catch (Exception e) {
                        Toast.makeText(EmailPassChangeActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    private void updatePassword(FirebaseUser user) {

        user.updatePassword(emailN).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(EmailPassChangeActivity.this, "Password successfully change", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), UserProfileActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    try {
                        throw task.getException();
                    } catch (Exception e) {
                        Toast.makeText(EmailPassChangeActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                Intent intent = new Intent(this, UserProfileActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
        }

        return super.onOptionsItemSelected(item);
    }
}