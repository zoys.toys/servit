package com.example.servit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.servit.databinding.ActivityConfirmOrderBinding;
import com.example.servit.databinding.ActivityOrderDetailBinding;
import com.google.android.material.appbar.MaterialToolbar;

import java.util.ArrayList;

public class OrderDetailActivity extends AppCompatActivity {

    TextView txtUserName, txtUserPhone, txtUserAddress, txtOrderDate, txtOrderTime, txtServiceName, txtExtraName, txtServicePrice, txtExtraPrice, txtTotalPrice, txtOrderTotal, txtUserNote;
    String userId, userName, userAddress, orderDate, orderTime, serviceName, extraName, userNote1, userPhone, catName;
    int servicePrice, extraPrice, totalPrice,  orderTotal;
    ActivityOrderDetailBinding binding;
    ArrayList<String> list;
    ArrayList<Integer> list2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOrderDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        MaterialToolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView txtToolbar = findViewById(R.id.txt_name);
        txtToolbar.setText("Order Detail");

        txtUserName = binding.txtName1;
        txtUserAddress = binding.txtAddress;
        txtUserPhone = binding.txtPhone;
        txtUserNote = binding.userNote;
        txtOrderDate = binding.txtDate;
        txtOrderTime = binding.txtTime;
        txtOrderTotal = binding.txtTotalUang;
        txtTotalPrice = binding.totalPrice;
        txtExtraName = binding.extraName;
        txtExtraPrice = binding.extraPrice;
        txtServicePrice = binding.servicePrice;
        txtServiceName = binding.serviceName;
        setAndgetData();
    }

    private void setAndgetData() {

        orderTotal = getIntent().getExtras().getInt("orderTotal");

        catName = getIntent().getExtras().getString("catName");
        userNote1 = getIntent().getExtras().getString("userNote");
        userName = getIntent().getExtras().getString("userName");
        userPhone = getIntent().getExtras().getString("userPhone");
        userAddress = getIntent().getExtras().getString("userAddress");
        orderDate = getIntent().getExtras().getString("orderDate");
        orderTime = getIntent().getExtras().getString("orderTime");
        serviceName = getIntent().getExtras().getString("serviceName");
//        extraName = getIntent().getExtras().getString("extraName");
        servicePrice = getIntent().getExtras().getInt("servicePrice");
        extraPrice = getIntent().getExtras().getInt("extraPrice");
        list = getIntent().getExtras().getStringArrayList("extraName");
        list2 = getIntent().getExtras().getIntegerArrayList("extraListPrice");


        txtUserName.setText(userName);
        txtUserAddress.setText(userAddress);
        txtUserPhone.setText(userPhone);
        txtUserNote.setText(userNote1);
        txtOrderDate.setText(orderDate);
        txtOrderTime.setText(orderTime);
        txtOrderTotal.setText("Rp. " + String.valueOf(orderTotal));
        txtTotalPrice.setText("Rp. " + String.valueOf(orderTotal));
//        txtExtraName.setText(extraName);

        txtExtraName.setText("");

        for (int i = 0; i < list.size(); i++){
            txtExtraName.append(list.get(i));
            txtExtraName.append("\n\n");
        }

//        txtExtraPrice.setText("Rp. " + String.valueOf(extraPrice));

        txtExtraPrice.setText("");

        for (int i = 0; i < list2.size(); i++){
            txtExtraPrice.append("Rp. "+list2.get(i).toString());
            txtExtraPrice.append("\n\n");
        }
//        txtExtraPrice.setText("Rp. " + String.valueOf(extraPrice));
        txtServicePrice.setText("Rp. " + String.valueOf(servicePrice));
        txtServiceName.setText(serviceName);

        Log.d("dsadas113131", userNote1);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                this.finish();
                return true;
//                Intent intent = new Intent(this, OrderUserInfoActivity.class);
////                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
////                finish();
////                moveTaskToBack(true);

        }

        return super.onOptionsItemSelected(item);
    }
}