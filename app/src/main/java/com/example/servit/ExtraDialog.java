package com.example.servit;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class ExtraDialog extends DialogFragment {

    EditText mInput;
    Context context;
    FirebaseDatabase fdata;
    DatabaseReference mDatabase;
    String orderID = "";
    TextView txtService;
    MaterialButton btnConfirm, btnBack;
    private static final String TAG = "CancelDialog";

    extraDialogListener listener;

    public interface extraDialogListener {
        void onSendValueClick(String value);
    }

    public ExtraDialog(extraDialogListener listener) {
        this.listener = listener;
    }

    public ExtraDialog() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_more, container, false);
        btnBack = view.findViewById(R.id.btn_back);
        btnConfirm = view.findViewById(R.id.btn_cancel);
        mInput = view.findViewById(R.id.text);
        fdata = FirebaseDatabase.getInstance();
        mDatabase = fdata.getReference("orders");
        txtService = view.findViewById(R.id.heading);

                btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick : Back");
                getDialog().dismiss();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick : Confirm");
//                orderID = getArguments().getString("orderId");

                String input = mInput.getText().toString();
                String regexStr = "^[1-9]\\d*(\\.\\d+)?$";



                if (!input.isEmpty()) {

                    if (input.matches(regexStr)) {

                        txtService.setText("Enter Service Hour");

                    } else {

                        txtService.setText("Enter Service");

                    }

                    listener.onSendValueClick(input);
                    getDialog().dismiss();

                } else {
                    Toast.makeText(getContext(), "User have to insert service", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (extraDialogListener) context;
        } catch (ClassCastException e) {
            Log.e(TAG, e.getMessage());
            throw new ClassCastException((getContext().toString()) +
                    " must implement Lisneter");
        }
    }
}
