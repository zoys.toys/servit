package com.example.servit;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.servit.model.UserAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegisterTabFragment extends Fragment {


    View objectSignUpFragment;
    private EditText userEmail, mobileNumber, userPass, userPassConfirm;
    private FirebaseAuth fAuth;
    private FirebaseDatabase fdata;
    private String userID;
    DatabaseReference databaseUser;
    private static final String TAG = "Register";
    ProgressBar progressBar;
    MaterialButton materialButton;
    private TextView txtWarnEmail, txtWarnMobileNumber, txtWarnPass, txtWarnPassConfirm;
    List<UserAccount> users;

    public RegisterTabFragment() {
        // Required empty public constructor
    }



    private void initElement() {
        try {

            Log.d(TAG, "initElement: Started");

            initView();

            materialButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressBar.setVisibility(View.VISIBLE);
                    if (validateData()) {
                        endWarning();
                        userRegister();

                    }
                }
            });

        } catch (Exception e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectSignUpFragment = inflater.inflate(R.layout.fragment_register_tab, container, false);
        initElement();

        return objectSignUpFragment;
    }

    private boolean validateData() {
        Log.d(TAG, "validateData: Started");

        if (!TextUtils.isEmpty(userEmail.getText()) && !TextUtils.isEmpty(mobileNumber.getText()) && !TextUtils.isEmpty(userPass.getText()) && !TextUtils.isEmpty(userPassConfirm.getText())) {

            endWarning();

            userEmail.setBackgroundResource(R.drawable.edit_text_bg);
            mobileNumber.setBackgroundResource(R.drawable.edit_text_bg);
            userPass.setBackgroundResource(R.drawable.edit_text_bg);
            userPassConfirm.setBackgroundResource(R.drawable.edit_text_bg);
            progressBar.setVisibility(View.GONE);

            if (!validateUserEmail(userEmail.getText().toString())) {
                txtWarnEmail.setVisibility(View.VISIBLE);
                userEmail.setBackgroundResource(R.drawable.edit_text_false_bg);
                txtWarnEmail.setText("Email not valid!");
                progressBar.setVisibility(View.GONE);
                return false;

            } else if (userPass.length() <= 3) {
                txtWarnPass.setVisibility(View.VISIBLE);
                userPass.setBackgroundResource(R.drawable.edit_text_false_bg);
                txtWarnPass.setText("Password is too short");
                progressBar.setVisibility(View.GONE);
                return false;

            } else if (!userPass.getText().toString().equals(userPassConfirm.getText().toString())) {
                txtWarnPassConfirm.setVisibility(View.VISIBLE);
                userPassConfirm.setBackgroundResource(R.drawable.edit_text_false_bg);
                txtWarnPassConfirm.setText("Password doesnt match");
                progressBar.setVisibility(View.GONE);
                return false;

            } else {
                progressBar.setVisibility(View.GONE);
                return true;
            }


        } else if (TextUtils.isEmpty(userEmail.getText()) || TextUtils.isEmpty(mobileNumber.getText()) || TextUtils.isEmpty(userPass.getText()) || TextUtils.isEmpty(userPassConfirm.getText())) {

            userEmail.setBackgroundResource(R.drawable.edit_text_false_bg);
            mobileNumber.setBackgroundResource(R.drawable.edit_text_false_bg);
            userPass.setBackgroundResource(R.drawable.edit_text_false_bg);
            userPassConfirm.setBackgroundResource(R.drawable.edit_text_false_bg);

            txtWarnEmail.setVisibility(View.VISIBLE);
            txtWarnEmail.setText("Enter your Email");
            txtWarnMobileNumber.setVisibility(View.VISIBLE);
            txtWarnMobileNumber.setText("Enter your Mobile Number");
            txtWarnPass.setVisibility(View.VISIBLE);
            txtWarnPass.setText("Enter your Password");
            txtWarnPassConfirm.setVisibility(View.VISIBLE);
            txtWarnPassConfirm.setText("Re enter your Password");

            if (!TextUtils.isEmpty(userEmail.getText())) {
                txtWarnEmail.setVisibility(View.GONE);
                userEmail.setBackgroundResource(R.drawable.edit_text_bg);
            }
            if (!TextUtils.isEmpty(mobileNumber.getText())) {
                txtWarnMobileNumber.setVisibility(View.GONE);
                mobileNumber.setBackgroundResource(R.drawable.edit_text_bg);
            }
            if (!TextUtils.isEmpty(userPass.getText())) {
                txtWarnPass.setVisibility(View.GONE);
                userPass.setBackgroundResource(R.drawable.edit_text_bg);
            }
            if (!TextUtils.isEmpty(userPassConfirm.getText())) {
                txtWarnPassConfirm.setVisibility(View.GONE);
                userPassConfirm.setBackgroundResource(R.drawable.edit_text_bg);
            }

            progressBar.setVisibility(View.GONE);
        }
//        else if (userEmail.getText().toString().isEmpty()) {
//            txtWarnEmail.setVisibility(View.VISIBLE);
//            txtWarnEmail.setText("Enter your Email");
//            return false;
//
//        }else if (mobileNumber.getText().toString().isEmpty()){
//            txtWarnMobileNumber.setVisibility(View.VISIBLE);
//            txtWarnMobileNumber.setText("Enter your Mobile Number");
//            return false;
//        }else if (userPass.getText().toString().isEmpty()){
//            txtWarnPass.setVisibility(View.VISIBLE);
//            txtWarnPass.setText("Enter your Password");
//            return false;
//        }else if(userPassConfirm.getText().toString().isEmpty()){
//            txtWarnPassConfirm.setVisibility(View.VISIBLE);
//            txtWarnPassConfirm.setText("Re enter your Password");
//            return false;
//        }
        return false;


    }

    private void endWarning() {
        txtWarnMobileNumber.setVisibility(View.GONE);
        txtWarnEmail.setVisibility(View.GONE);
        txtWarnPass.setVisibility(View.GONE);
        txtWarnPassConfirm.setVisibility(View.GONE);
    }

    private void userRegister() {
        Log.d(TAG, "userRegister: Started");

        final String email = userEmail.getText().toString().trim();
        final String password = userPass.getText().toString().trim();
        final long phone = Long.parseLong(mobileNumber.getText().toString());
        final String name = "";
        final String address = "";


        fAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "createUserWithEmail:success");
//                    Toast.makeText(getContext(), "Register Successfully", Toast.LENGTH_SHORT).show();
                    databaseUser = fdata.getReference("users");
//                            child(userID);
                    userID = fAuth.getCurrentUser().getUid();
//                    String id = databaseUser.push().getKey();
                    UserAccount users = new UserAccount(email, phone, userID, name, address);
                    databaseUser.child(userID).setValue(users).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void unused) {
                            progressBar.setVisibility(View.GONE);
                            userEmail.setText(null);
                            userPass.setText(null);
                            mobileNumber.setText(null);
                            userPassConfirm.setText(null);
                        }
                    });
                    Toast.makeText(getContext(), "Account Created Successfuly", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getContext(), ViewPageLogRegis.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
//                    Log.w(TAG, "createUserWithEmail:failure", task.getException());
//                    Toast.makeText(getContext(), "Error ! " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        txtWarnEmail.setVisibility(View.VISIBLE);
                        userEmail.setBackgroundResource(R.drawable.edit_text_false_bg);
                        txtWarnEmail.setText("Email already exist");
                        userEmail.requestFocus();
                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                    }
                }
                fAuth.signOut();
            }
        });
    }

    private static boolean validateUserEmail(String email) {
        return email.contains("@") && email.contains(".");
    }

    private void initView() {
        userEmail = objectSignUpFragment.findViewById(R.id.email);
        userPass = objectSignUpFragment.findViewById(R.id.password);
        mobileNumber = objectSignUpFragment.findViewById(R.id.mobileNumber);
        userPassConfirm = objectSignUpFragment.findViewById(R.id.passwordConfirm);
        materialButton = objectSignUpFragment.findViewById(R.id.materialButton);

        txtWarnEmail = objectSignUpFragment.findViewById(R.id.txtWarnEmail);
        txtWarnMobileNumber = objectSignUpFragment.findViewById(R.id.txtWarnMobileNumber);
        txtWarnPass = objectSignUpFragment.findViewById(R.id.txtWarnPass);
        txtWarnPassConfirm = objectSignUpFragment.findViewById(R.id.txtWarnPassConfirm);

        fAuth = FirebaseAuth.getInstance();
        fdata = FirebaseDatabase.getInstance();
        progressBar = objectSignUpFragment.findViewById(R.id.progress_register);
        users = new ArrayList<>();
    }

}




