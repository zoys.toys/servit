package com.example.servit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatToggleButton;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.servit.adapter.ExtraServiceAdapter;
import com.example.servit.adapter.ServiceOrderAdapter;
import com.example.servit.databinding.ActivityOrderBinding;
import com.example.servit.model.ExtraServiceModel;
import com.example.servit.model.OrderCategoryModel;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class OrderActivity2 extends AppCompatActivity {

    private DatePickerDialog datePickerDialog;
    private MaterialButton dateButton, timeButton;
    private AppCompatToggleButton btn_1, btn_2, btn_3, btn_4, btn_5, btn_6;
    private static final String TAG = "OrderActivity";
    private ArrayList<OrderCategoryModel> orderCategoryModels;
    private ArrayList<ExtraServiceModel> extraServiceModels;
    DatabaseReference mOrderDatabase, mExtraDatabase;
    FirebaseDatabase fData;
    RecyclerView recyclerViewOrder, recyclerViewExtraService;
    ServiceOrderAdapter serviceOrderAdapter;
    ExtraServiceAdapter extraServiceAdapter;
    ValueEventListener valueEventListener;
    TextView txt_total_uang, txt_uang_extra, txt_uang_service;
    ImageView arrow_icon;
    private int currentIndex;
    int hour, minute;
    private String totalUang;


    public OrderActivity2() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order2);
        initDatePicker();

        txt_total_uang = findViewById(R.id.txt_total_uang);

        dateButton = findViewById(R.id.btn_date);
        timeButton = findViewById(R.id.btn_time);
        btn_1 = findViewById(R.id.btn_1_jam);
        btn_2 = findViewById(R.id.btn_2_jam);
        btn_3 = findViewById(R.id.btn_3_jam);
        btn_4 = findViewById(R.id.btn_4_jam);
        btn_5 = findViewById(R.id.btn_5_jam);
        btn_6 = findViewById(R.id.btn_6_jam);

        CompoundButton.OnCheckedChangeListener toggleListener = new CompoundButton.OnCheckedChangeListener() {

            boolean avoidRecursions = false;

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (avoidRecursions) return;
                avoidRecursions = true;

                if (!isChecked) {

                    if (buttonView == btn_1){
                        totalUang = "1000";
                        buttonView.setChecked(true);
                        avoidRecursions = false;
                        txt_total_uang.setText(totalUang);
                    }else if (buttonView == btn_2){
                        totalUang = "2000";
                        txt_total_uang.setText(totalUang);
                        buttonView.setChecked(true);
                        avoidRecursions = false;

                    }else if (buttonView == btn_3){
                        totalUang = "3000";
                        txt_total_uang.setText(totalUang);
                        buttonView.setChecked(true);
                        avoidRecursions = false;

                    }else if (buttonView == btn_4){
                        totalUang = "4000";
                        txt_total_uang.setText(totalUang);
                        buttonView.setChecked(true);
                        avoidRecursions = false;

                    }else if (buttonView == btn_5){
                        totalUang = "5000";
                        txt_total_uang.setText(totalUang);
                        buttonView.setChecked(true);
                        avoidRecursions = false;
                    }else if (buttonView == btn_6)
                        totalUang = "6000";
                    txt_total_uang.setText(totalUang);
                    buttonView.setChecked(true);
                    avoidRecursions = false;
                    return;
                }

                if (buttonView != btn_1 && btn_1.isChecked()) {

                    btn_1.setChecked(false);
                }

                else if (buttonView != btn_2 && btn_2.isChecked()){

                    btn_2.setChecked(false);
                }
                else if (buttonView != btn_3 && btn_3.isChecked()){

                    btn_3.setChecked(false);
                }
                else if (buttonView != btn_4 && btn_4.isChecked()){

                    btn_4.setChecked(false);
                }
                else if (buttonView != btn_5 && btn_5.isChecked()){

                    btn_5.setChecked(false);
                }
                else if (buttonView != btn_6 && btn_6.isChecked()) {

                    btn_6.setChecked(false);
                }

                avoidRecursions = false;

            }
        };

        String goScore = txt_total_uang.getText().toString();
        Log.d("eeeeeeeeeee", goScore);
        txt_total_uang.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

//                String eee = txt_total_uang.getText().toString();
//                OrderCategoryModel orderCategoryModel =
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        txt_total_uang.setText(totalUang);

        btn_1.setOnCheckedChangeListener(toggleListener);
        btn_2.setOnCheckedChangeListener(toggleListener);
        btn_3.setOnCheckedChangeListener(toggleListener);
        btn_4.setOnCheckedChangeListener(toggleListener);
        btn_5.setOnCheckedChangeListener(toggleListener);
        btn_6.setOnCheckedChangeListener(toggleListener);

    }

    private void initDatePicker() {
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = makeDateString(dayOfMonth, month, year);
                dateButton.setText(date);
//                dateButton.setText(getTodaysDate());
            }
        };

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        int style = AlertDialog.THEME_HOLO_LIGHT;

        datePickerDialog = new DatePickerDialog(this, style, dateSetListener, year, month, day);
//        datePickerDialog.getDatePicker().setMinDate(Instant.now().toEpochMilli());
    }

    private String getTodaysDate() {

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        month = month + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);
        return makeDateString(day, month, year);
    }

    private String makeDateString(int dayOfMonth, int month, int year) {
        return getMonthFormat(month) + " " + dayOfMonth + " " + year;
    }

    private String getMonthFormat(int month) {
        if (month == 1) {
            return "JAN";
        } else if (month == 2) {
            return "FEB";
        } else if (month == 3) {
            return "MAR";
        } else if (month == 4) {
            return "APR";
        } else if (month == 5) {
            return "MAY";
        } else if (month == 6) {
            return "JUN";
        } else if (month == 7) {
            return "JUL";
        } else if (month == 8) {
            return "AUG";
        } else if (month == 9) {
            return "SEP";
        } else if (month == 10) {
            return "OCT";
        } else if (month == 11) {
            return "NOV";
        } else if (month == 12) {
            return "DEX";
        }
        return "Jan";
    }

    public void openDatePicker(View view) {
        datePickerDialog.show();
    }

    public void openTimerPicker(View view) {
        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                hour = hourOfDay;
                minute = minute;
                timeButton.setText(String.format(Locale.getDefault(), "%02d:%02d", hourOfDay, minute));
            }
        };

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, onTimeSetListener, hour, minute, true);

        timePickerDialog.setTitle("Select Time");
        timePickerDialog.show();
    }
}