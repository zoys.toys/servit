package com.example.servit;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class CancelDialog extends DialogFragment {

    EditText mInput;
    Context context;
    FirebaseDatabase fdata;
    DatabaseReference mDatabase;
    String orderID = "";
    MaterialButton btnConfirm, btnBack;
    private static final String TAG = "CancelDialog";


//    public interface CancelDialogListener {
//        void onDialogPositiveClick(String dialogFragment);
//
//    }

//    public CancelDialog(CancelDialogListener listener) {
//        this.listener = listener;
//    }

    public CancelDialog() {

    }

//    public CancelDialogListener listener;


//    @Override
//    public void onAttach(@NonNull Context context) {
//        super.onAttach(context);
//
//        try {
//            listener = (CancelDialogListener) context;
//        } catch (ClassCastException e) {
//            Log.e(TAG, e.getMessage());
//            throw new ClassCastException(context + " must implementDialogListener");
//        }
//    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_cancel, container, false);
        btnBack = view.findViewById(R.id.btn_back);
        btnConfirm = view.findViewById(R.id.btn_cancel);
        mInput = view.findViewById(R.id.text);
        fdata = FirebaseDatabase.getInstance();
        mDatabase = fdata.getReference("orders");


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick : Back");
                getDialog().dismiss();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick : Confirm");
                orderID = getArguments().getString("orderId");

                String input = mInput.getText().toString();


                if (!input.isEmpty()) {

                    Map<String, Object> value = new HashMap<>();
                    value.put("status", 3);
                    value.put("Cancel Note", input);

                    mDatabase.child(orderID).updateChildren(value);
//                    listener.onDialogPositiveClick(input);
                    getDialog().dismiss();

                } else {
                    Toast.makeText(getContext(), "User have to insert reason", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;

    }

//    @Override
//    public void sendValue(String value) {
//
//        orderID = value;
//        Toast.makeText(context, value, Toast.LENGTH_SHORT).show();
//
//    }

    //    @NonNull
//    @Override
//    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//
//        LayoutInflater inflater = requireActivity().getLayoutInflater();
//
//        builder.setView(inflater.inflate(R.layout.dialog_cancel, null))
//                .setPositiveButton("Enter", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        listener.onDialogPositiveClick(mInput.getText().toString());
//
//                        String input = mInput.get
//
//                    }
//                })
//                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });
//        return builder.create();
//    }
}
