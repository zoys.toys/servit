package com.example.servit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.servit.databinding.ActivityOrderUserInfoBinding;
import com.example.servit.databinding.ActivityUserProfileBinding;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

public class OrderUserInfoActivity extends AppCompatActivity {

    EditText userName, userNumber, userAddress, userNotes;
    TextView txtTotal;
    ImageView arrow_icon;
    DatabaseReference mDatabase;
    FirebaseDatabase fdata;
    FirebaseAuth fauth;
    FirebaseUser user;
    ActivityOrderUserInfoBinding binding;
    ArrayList<String> list;
    ArrayList<Integer> list2;
    private String userId, orderDate, orderTime, userN, userNN, userA, serviceName, extraName, userP, catName;
    private int orderTotal, servicePrice, extraPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOrderUserInfoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        MaterialToolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView textView = toolbar.findViewById(R.id.txt_name);
        textView.setText("Order Info");
        fauth = FirebaseAuth.getInstance();
        fdata = FirebaseDatabase.getInstance();
        user = fauth.getCurrentUser();
        mDatabase = fdata.getReference("users");

        txtTotal = binding.txtTotalUang;
        userName = binding.editTextTextPersonName;
        userNumber = binding.editTextPhone;
        userAddress = binding.txtAddress1;
        userNotes = binding.txtNote;
        arrow_icon = binding.imgBtnNext;

        userId = user.getUid();

        list = new ArrayList<>();
        list2= new ArrayList<>();

        setData();

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                Intent intent = new Intent(this, OrderActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
//                finish();
        }

        return super.onOptionsItemSelected(item);
    }


    private void setData() {

        orderTotal = getIntent().getExtras().getInt("currentTotal");
        orderDate = getIntent().getExtras().getString("currentDate");
        orderTime = getIntent().getExtras().getString("currentTime");

        catName = getIntent().getExtras().getString("catName");
        serviceName = getIntent().getExtras().getString("serviceName");
        list = getIntent().getExtras().getStringArrayList("extraName");
        list2 = getIntent().getExtras().getIntegerArrayList("extraListPrice");
        servicePrice = getIntent().getExtras().getInt("servicePrice");
        extraPrice = getIntent().getExtras().getInt("extraPrice");


        mDatabase.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (!snapshot.child("name").getValue().toString().equals("") || !snapshot.child("address").getValue().toString().equals("")) {
                    Log.d("eeeeeeee1212121", "ga ada");
                    userName.setText(Objects.requireNonNull(snapshot.child("name").getValue()).toString());
                    userAddress.setText(Objects.requireNonNull(snapshot.child("address").getValue()).toString());
                    userNumber.setText(Objects.requireNonNull(snapshot.child("phoneNumber").getValue()).toString());
                    txtTotal.setText("Rp. " + String.valueOf(orderTotal));

                } else {
                    userNumber.setText(Objects.requireNonNull(snapshot.child("phoneNumber").getValue()).toString());
                    txtTotal.setText("Rp. " + String.valueOf(orderTotal));

                    Log.d("eeeeeeee1212121313131", "ada");

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }

        });

        newArrow();

    }

    private void getData() {

        userN = userName.getText().toString();
        userP = userNumber.getText().toString();
        userA = userAddress.getText().toString();

    }

    private void newArrow() {
        arrow_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
                userNN = userNotes.getText().toString();
                Bundle bundle = new Bundle();
                bundle.putString("catName", catName);
                bundle.putInt("servicePrice", servicePrice);
                bundle.putInt("extraPrice", extraPrice);
                bundle.putString("serviceName", serviceName);
                bundle.putStringArrayList("extraName", list);
                bundle.putIntegerArrayList("extraListPrice", list2);
                bundle.putString("orderDate", orderDate);
                bundle.putString("orderTime", orderTime);
                bundle.putString("userName", userN);
                bundle.putString("userNote", userNN);
                bundle.putString("userAddress", userA);
                bundle.putString("userPhone", userP);
                bundle.putInt("orderTotal", orderTotal);
                Log.d("dadasdasdasdasdas", userP + userA + userN + userNN);
                Intent intent = new Intent(getApplicationContext(), ConfirmOrderActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });
    }
}