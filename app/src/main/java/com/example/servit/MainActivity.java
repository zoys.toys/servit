package com.example.servit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.servit.databinding.ActivityMainBinding;
import com.example.servit.repo.ServitRepo;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity{

    private ActivityMainBinding binding;
    private FirebaseAuth mAuth;
    String userID;
    private static final String TAG = "MainActivity";
    ServitRepo repo;
    androidx.appcompat.widget.Toolbar toolbar;

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart : started");
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            Log.w(TAG, "onStart : no user login");
            Intent intent = new Intent(this, ViewPageLogRegis.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
//            Toast.makeText(this, "Account Created Successfuly" + userID, Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Log.d(TAG, "onStart : user found");

        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        BottomNavigationView navigationView = findViewById(R.id.bottom_navigatin_view);
//
//        NavController navController = Navigation.findNavController(this, R.id. nav_fragment);
//        NavigationUI.setupWithNavController(navigationView, navController);

        mAuth = FirebaseAuth.getInstance();
        Log.d(TAG, "onCreate : started");

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView navigationView = findViewById(R.id.bottom_navigatin_view);

//        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
//                R.id.navigation_home, R.id.navigation_order, R.id.navigation_provider_list, R.id.navigation_profile)
//                .build();

        NavController navController = Navigation.findNavController(this, R.id. nav_fragment);
//        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.bottomNavigatinView, navController);

//        navigationView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Navigation.findNavController(v).navigate();
//            }
//        });



//        userID = mAuth.getCurrentUser().getUid();
//
//        FirebaseUser currentUser = mAuth.getCurrentUser();
//        if (currentUser == null) {
//            Intent intent = new Intent(this, ViewPageLogRegis.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
////            Toast.makeText(this, "Account Created Successfuly" + userID, Toast.LENGTH_SHORT).show();
//            finish();
//        } else {

//            Toast.makeText(this, userID, Toast.LENGTH_SHORT).show();
//            Log.w(TAG, "Aneh");
//        }


//        BottomNavigationView navView = findViewById(R.id.bottom_navigatin_view);


        //Initialize the bottom navigation view
//        //create bottom navigation view object
//        BottomNavigationView bottomNavigationView = findViewById<BottomNavigationView
//                >(R.id.bottom_navigatin_view)
//                val navController = findNavController(R.id.nav_fragment)
//        bottomNavigationView.setupWithNavController(navController);
//        binding = ActivityMainBinding.inflate(getLayoutInflater());
//        setContentView(binding.getRoot());
//
//        BottomNavigationView navView = findViewById(R.id.bottom_navigatin_view);
//        // Passing each menu ID as a set of Ids because each
//        // menu should be considered as top level destinations.
//        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
//                R.id.navigation_home, R.id.navigation_order, R.id.navigation_provider_list, R.id.navigation_profile)
//                .build();
//        NavController navController = Navigation.findNavController(this, R.id.nav_fragment);
////        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
//        NavigationUI.setupWithNavController(navView, navController);

    }


//    @Override
//    public void onDialogPositiveClick(String dialogFragment) {
//
//    }
}