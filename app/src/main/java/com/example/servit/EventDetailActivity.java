package com.example.servit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.example.servit.model.eventModel;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class EventDetailActivity extends AppCompatActivity {

    private TextView txt_title, txt_desc, txt;
    private ImageView img_event;
    private String eventId, eventName, eventTitle, eventDesc, imgUrl;
    DatabaseReference mDatabase;
    FirebaseDatabase fData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        MaterialToolbar txt_event_name = findViewById(R.id.toolbar);
        setSupportActionBar(txt_event_name);
        txt = txt_event_name.findViewById(R.id.txt_name);
        txt.setText("Event Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fData = FirebaseDatabase.getInstance();
        initViews();
        setData();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setData() {

        eventId = getIntent().getExtras().getString("id");
        eventName = getIntent().getExtras().getString("eventName");
        eventTitle = getIntent().getExtras().getString("title");
        eventDesc = getIntent().getExtras().getString("longDesc");
        imgUrl = getIntent().getExtras().getString("imageUrl");
        mDatabase = fData.getReference().child("events");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Glide.with(getApplicationContext()).load(imgUrl).into(img_event);

                txt_title.setText(eventTitle);
                txt_desc.setText(eventDesc);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    private void initViews() {
        txt_desc = findViewById(R.id.txt_desc);
        txt_title = findViewById(R.id.txt_title_name);

        img_event = findViewById(R.id.img_event);
    }
}