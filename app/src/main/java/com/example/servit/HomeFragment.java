package com.example.servit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.servit.adapter.CategoryAdapter;
import com.example.servit.adapter.EventAdapter;
import com.example.servit.adapter.ServiceAdapter;
import com.example.servit.databinding.EventViewListBinding;
import com.example.servit.databinding.HomeFragmentBinding;
import com.example.servit.model.CatModel;
import com.example.servit.model.ServiceModel;
import com.example.servit.model.eventModel;
import com.example.servit.repo.ServitRepo;
import com.example.servit.viewModels.HomeViewModel;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private HomeFragmentBinding binding;
    ServitRepo repo;
    MaterialButton logbut;
    private static final String TAG = "HomeFragment";
    private FirebaseAuth mAuth;
    HomeViewModel mViewModel;
    RecyclerView recyclerViewEvent, recyclerViewCategory, recyclerViewFav;
    DatabaseReference mEventDatabase, mCatDatabase, mFavDatabase;
    EventAdapter eventAdapter;
    CategoryAdapter categoryAdapter;
    ServiceAdapter serviceAdapter;
    IndefinitePagerIndicator pagerIndicator;
    FirebaseDatabase fData;
    private ArrayList<CatModel> catModels;
    private ArrayList<ServiceModel> serviceModels;
    private ArrayList<eventModel> eventModels;

    public HomeFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        mViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        binding = HomeFragmentBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        mAuth = FirebaseAuth.getInstance();
        fData = FirebaseDatabase.getInstance();
        mEventDatabase = fData.getReference("events");
        mCatDatabase = fData.getReference("categories");
        mFavDatabase = fData.getReference("provider");
        catModels = new ArrayList<>();
        serviceModels = new ArrayList<>();
        eventModels = new ArrayList<>();

        MaterialToolbar toolbar = binding.toolbar.getRoot();
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        TextView txt = toolbar.findViewById(R.id.txt_name);
        txt.setText("");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(R.drawable.servit);


//        final TextView textView = binding.homeTxt;
//        mViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);

        recyclerViewEvent = binding.recyclesEvent;
        recyclerViewCategory = binding.recyclesCate;
        recyclerViewFav = binding.recyclesFav;
        pagerIndicator = binding.recyclerviewPagerIndicator;

        recyclerViewFav.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewEvent.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewCategory.setLayoutManager(new GridLayoutManager(getContext(), 4));

        pagerIndicator.attachToRecyclerView(recyclerViewEvent);


//        FirebaseRecyclerOptions<eventModel> options = new FirebaseRecyclerOptions.Builder<eventModel>()
//                .setQuery(mEventDatabase, eventModel.class)
//                .build();

        mEventDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    eventModel model = dataSnapshot.getValue(eventModel.class);
                    eventModels.add(model);
                }


                eventAdapter = new EventAdapter(getContext(), eventModels);
                recyclerViewEvent.setAdapter(eventAdapter);
                eventAdapter.setEventModels(EventAdapter.getInstance().getAllEvent());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        mCatDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    CatModel catModel = dataSnapshot.getValue(CatModel.class);
                    catModels.add(catModel);
                }

                categoryAdapter = new CategoryAdapter(getContext(), catModels);
                recyclerViewCategory.setAdapter(categoryAdapter);
                categoryAdapter.setCatModels(CategoryAdapter.getInstance().getAllCat());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w(TAG, error.getMessage());
            }
        });

        mFavDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    ServiceModel serviceModel = dataSnapshot.getValue(ServiceModel.class);
                    if (serviceModel.getFavorite().equals(true)) {

                        serviceModels.add(serviceModel);

                    }

                }

                serviceAdapter = new ServiceAdapter(getContext(), serviceModels);
                recyclerViewFav.setAdapter(serviceAdapter);
                serviceAdapter.setServiceModels(ServiceAdapter.getInstance().getAllService());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.w(TAG, error.getMessage());
            }
        });


        return root;
    }

//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        mViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
//        // TODO: Use the ViewModel
//
//    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}