package com.example.servit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.ImageViewCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.servit.databinding.ActivityServiceProviderDetailBinding;
import com.example.servit.model.ServiceModel;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ServiceProviderDetailActivity extends AppCompatActivity {

    private TextView providerName, providerService, providerAddress, providerDesc;
    private ImageView providerImg;
    private String pName,pAddress,pProvice, pUrl, pLongDesc;
    DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_provider_detail);

        MaterialToolbar toolbar = findViewById(R.id.toolbar_layout);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        TextView txtToolbar = findViewById(R.id.txt_name);
//        txtToolbar.setText("Order Detail");

        initViews();
        setData();



    }

    private void setData(){

        pName = getIntent().getExtras().getString("serviceName");
        pAddress = getIntent().getExtras().getString("serviceAddress");
        pProvice = getIntent().getExtras().getString("serviceProvide");
        pUrl = getIntent().getExtras().getString("serviceUrl");
        pLongDesc = getIntent().getExtras().getString("serviceLongDesc");
        mDatabase = FirebaseDatabase.getInstance().getReference("provider");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                Glide.with(getApplicationContext()).load(pUrl).into(providerImg).toString();

                providerName.setText(pName);
                providerService.setText(pProvice);
                providerAddress.setText(pAddress);
                providerDesc.setText(pLongDesc);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }

    private void initViews() {
        providerName = findViewById(R.id.txt_provider_name);
        providerService = findViewById(R.id.txt_service_provide);
        providerAddress = findViewById(R.id.txt_address_provider);
        providerDesc = findViewById(R.id.txt_long_desc);
        providerImg = findViewById(R.id.imageView2);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                this.finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}