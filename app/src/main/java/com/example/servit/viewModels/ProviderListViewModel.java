package com.example.servit.viewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ProviderListViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    private final MutableLiveData<String> mText;

    public ProviderListViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Provider");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
