package com.example.servit.viewModels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.servit.repo.ServitRepo;

public class SignUpViewModel extends ViewModel {

        private ServitRepo repo;
        private final MutableLiveData<String> status;

        public SignUpViewModel(MutableLiveData<String> status) {
                this.status = status;
        }
}