package com.example.servit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.databinding.DataBindingComponent;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.bumptech.glide.Glide;
import com.example.servit.databinding.ActivityUserProfileBinding;
import com.example.servit.model.UserAccount;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserProfileActivity extends AppCompatActivity{

    ActivityUserProfileBinding binding;
    TextView textTool;
    EditText pName, pEmail, pNumber, pAddress;
    MaterialButton btnSave, btnchange;
    CircleImageView pImg;
    FirebaseAuth fauth;
    DatabaseReference mDatabase, mDatabasePic;
    FirebaseDatabase fdata;
    StorageReference sStorage;
    FirebaseUser user;
    String userId;
    private Uri uriImage;
    private static final int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_user_profile);
        binding = ActivityUserProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        MaterialToolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fauth = FirebaseAuth.getInstance();
        fdata = FirebaseDatabase.getInstance();
        user = fauth.getCurrentUser();

        textTool = binding.toolbar.txtName;
        pName = binding.txtNameP;
        pNumber = binding.txtMobileNumber;
        pAddress = binding.txtAddress;
        btnSave = binding.btnSave;
        pImg = binding.pImgae;
        btnchange = binding.btnChange;


        textTool.setText("Edit Profile");
        userId = user.getUid();
        mDatabase = fdata.getReference("users").child(userId);
        mDatabasePic = fdata.getReference("userPics");
        sStorage = FirebaseStorage.getInstance().getReference("userPics");
        Uri uri = user.getPhotoUrl();


        Glide.with(getApplicationContext()).load(uri).dontAnimate().into(pImg);


        btnchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EmailPassChangeActivity.class);
                startActivity(intent);
            }
        });

        pImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                pName.setText(snapshot.child("name").getValue().toString());
//                pEmail.setText(snapshot.child("email").getValue().toString());
                pNumber.setText(snapshot.child("phoneNumber").getValue().toString());
                pAddress.setText(snapshot.child("address").getValue().toString());
//                String eee = snapshot.getValue(UserAccount.class).getEmail();
//                pEmail.setText(eee);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = pName.getText().toString();
                long number = Long.parseLong(pNumber.getText().toString());
                String address = pAddress.getText().toString();

                updateData(name, number, address);
                uploadPic();

            }
        });



    }

    private void uploadPic() {
        if (uriImage != null){

            StorageReference fileReference = sStorage.child(userId + "."  + getFileExtension(uriImage));

            fileReference.putFile(uriImage).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    fileReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Uri downloadUri = uri;
                            user = fauth.getCurrentUser();

                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setPhotoUri(downloadUri).build();
                            user.updateProfile(profileUpdates);
                        }
                    });
                    recreate();
                }
            });
        }
    }

    private String getFileExtension(Uri uriImage) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(contentResolver.getType(uriImage));
    }

    private void updateData(String name, long number, String address) {
        if(pName.getText().toString().isEmpty() && pAddress.getText().toString().isEmpty() && pNumber.getText().toString().isEmpty()){
            Toast.makeText(getApplicationContext(), "One or many fields are empty", Toast.LENGTH_SHORT).show();
            return;
        }

        Map<String, Object> edited = new HashMap<>();
        edited.put("name", name);
        edited.put("phoneNumber", number);
        edited.put("address", address);

        mDatabase.updateChildren(edited).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(UserProfileActivity.this, "Profile Updated", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(UserProfileActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void openFileChooser(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            uriImage = data.getData();
            pImg.setImageURI(uriImage);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case android.R.id.home:

                this.finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}