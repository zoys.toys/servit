package com.example.servit;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.servit.databinding.ProfileFragmentBinding;
import com.example.servit.viewModels.ProfileViewModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment {

    private ProfileViewModel mViewModel;
    private ProfileFragmentBinding binding;
    ConstraintLayout proLayout;
    CircleImageView profileImage;
    FirebaseDatabase fdata;
    FirebaseAuth fauth;
    private String userId, userUrl;
    TextView pName;
    private DatabaseReference mDatabase;
    StorageReference storageReference;
    FirebaseUser user;
    MaterialButton logoutbtn, aboutbtn;

    public ProfileFragment(){

    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
//        mViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        binding = ProfileFragmentBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        fdata = FirebaseDatabase.getInstance();
        fauth = FirebaseAuth.getInstance();
        mDatabase = fdata.getReference();
        pName = binding.txtName;
        profileImage = binding.profileImg;


//        TODO buat ini
//        StorageReference profileRef = storageReference.child("users/" + fauth.getCurrentUser().getUid() + "/profile.jpg");
//        profileRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
//            @Override
//            public void onSuccess(Uri uri) {
//                Glide.with(getContext()).load(uri).dontAnimate().into(profileImage);
//            }
//        });

        userId = fauth.getCurrentUser().getUid();
        Uri userUrl = fauth.getCurrentUser().getPhotoUrl();
        Glide.with(getContext()).load(userUrl).dontAnimate().into(profileImage);

        mDatabase = fdata.getReference().child("users").child(userId);
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                if(!snapshot.child("name").getValue().toString().isEmpty()){
                    pName.setText(snapshot.child("name").getValue().toString());
                }else {
                    pName.setText(snapshot.child("email").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        proLayout = binding.profileLayout;
        proLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), UserProfileActivity.class);
                startActivity(intent);
            }
        });

//        final TextView textView = binding.txtProfile;
//        mViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);

        logoutbtn = binding.materialButton2;
        logoutbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fauth.signOut();
                Intent intent = new Intent(getContext(), ViewPageLogRegis.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        aboutbtn = binding.materialButton3;
        aboutbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AboutAppActivity.class);
                startActivity(intent);
            }
        });

        return root;
    }

}